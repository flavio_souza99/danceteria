﻿using Nsf.trabalho.Telas.Cliente;
using Nsf.trabalho.Telas.Folha_de_pagamento;
using Nsf.trabalho.Telas.Fornecedor;
using Nsf.trabalho.Telas.Funcionario;
using Nsf.trabalho.Telas.Pedido;
using Nsf.trabalho.Telas.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            VerificarPermissoes();
        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.PermissaoProduto == false)
            {
                produtoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoCliente == false)
            {
                clienteToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoEstoque == false)
            {
                estoqueToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoPedido == false)
            {
                pedidoToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFolhaPagamento == false)
            {
                folhaDePagamentoToolStripMenuItem.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFornecedor == false)
            {
                fornecedorToolStripMenuItem.Enabled = false;
            }


            if (UserSession.UsuarioLogado.PermissaoFluxoCaixa == false)
            {
                fluxoDeCaixaToolStripMenuItem1.Enabled = false;
            }

            if (UserSession.UsuarioLogado.PermissaoFuncionario == false)
            {
                funcionarioToolStripMenuItem.Enabled = false;
            }

        }

        private void frmPedidoCadastrar_Load(object sender, EventArgs e)
        {
          
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmlogar retroceder = new frmlogar();
            this.Hide();
            retroceder.ShowDialog();            
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmCadastrarProdutos ir = new frmCadastrarProdutos();
            this.Hide();
            ir.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente ir = new frmCadastrarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void sToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void fecharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void minimizarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void logoufToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmlogar retroceder = new frmlogar();
            this.Close();
            retroceder.ShowDialog();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente ir = new frmCadastrarCliente();
            ir.ShowDialog();
        }

        private void recarregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSplash ir = new frmSplash();
            this.Hide();
            ir.ShowDialog();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void produtoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrarProdutos ir = new frmCadastrarProdutos();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarProduto ir = new frmConsultarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmAlterarProduto ir = new frmAlterarProduto();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmConsultarEstoque ir = new frmConsultarEstoque();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmCadastrarFornecedor ir = new frmCadastrarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmCadastrarFuncionario ir = new frmCadastrarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmCadastrarPedido ir = new frmCadastrarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem6_Click(object sender, EventArgs e)
        {
            frmConsultarFluxodeCaixa ir = new frmConsultarFluxodeCaixa();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmConsultarFolhadePagamento ir = new frmConsultarFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmFolhadePagamento ir = new frmFolhadePagamento();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmConsultarPedido ir = new frmConsultarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void fornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void consultarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            frmConsultarFuncioanario ir = new frmConsultarFuncioanario();
            this.Hide();
            ir.ShowDialog();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void alterarToolStripMenuItem7_Click(object sender, EventArgs e)
        {
            frmAlterarFuncionario ir = new frmAlterarFuncionario();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmAlterarFolhaPagamento ir = new frmAlterarFolhaPagamento();
            this.Close();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmAlterarFornecedor ir = new frmAlterarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void realizarPedidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedirFornecedor ir = new frmPedirFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarPedidoAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConsultarPedidoFornecedor ir = new frmConsultarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarPedidoFeitoAoFornecedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarPedidoFornecedor ir = new frmAlterarPedidoFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarCliente ir = new frmConsultarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void alterarToolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmAlterarCliente ir = new frmAlterarCliente();
            this.Hide();
            ir.ShowDialog();
        }

        private void realizarPedidoAoFornececedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmPedirFornecedor ir = new frmPedirFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblData2.Text = DateTime.Now.ToLongDateString();
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        private void alterarToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmAlterarPedido ir = new frmAlterarPedido();
            this.Hide();
            ir.ShowDialog();
        }

        private void cadastrarToolStripMenuItem5_Click(object sender, EventArgs e)
        {

        }
    }
}
