﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Salvar(dto);
        }

        public List<ClienteDTO> Consultar(string nome)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            ClienteDataBase db = new ClienteDataBase();
            db.Remover(id);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.Listar();
        }


        public void Alterar(ClienteDTO dados)
        {
            ClienteDataBase db = new ClienteDataBase();
            db.Alterar(dados);
        }

        public ClienteDTO ListarPorId(int idCliente)
        {
            ClienteDataBase db = new ClienteDataBase();
            return db.ListarPorId(idCliente);
        }

    }
}
