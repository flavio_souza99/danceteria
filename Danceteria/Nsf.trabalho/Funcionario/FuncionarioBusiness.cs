﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Funcionario
{
    class FuncionarioBusiness
    {
        public FuncionarioDTO Logar(string email, string senha)
        {

            if (email == string.Empty)
            {
                throw new ArgumentException("Digite o email");
            }

            if (senha == string.Empty)
            {
                throw new ArgumentException("Digite a senha");
            }

            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Logar(email, senha);
        }


        public int Salvar(FuncionarioDTO dto)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Salvar(dto);
        }

        public List<FuncionarioDTO> Consultar(string nome)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Consultar(nome);
        }

        public void Remover(int id)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            db.Remover(id);
        }

        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.Listar();
        }


        public void Alterar(FuncionarioDTO dados)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            db.Alterar(dados);
        }

        public FuncionarioDTO ListarPorId(int idFuncionario)
        {
            FuncionarioDataBase db = new FuncionarioDataBase();
            return db.ListarPorId(idFuncionario);
        }

    }
}
