﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.Pedido_Compra
{
    class PedidoCompraDTO
    {
        public int Id { get; set; }
        public int QtdProduto { get; set; }
        public int FornecedorId { get; set; }
        public int ProdutoId { get; set; }
        public DateTime DtPedido { get; set; }
    }
}
