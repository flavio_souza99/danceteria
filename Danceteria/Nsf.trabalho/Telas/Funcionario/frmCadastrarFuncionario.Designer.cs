﻿namespace Nsf.trabalho
{
    partial class frmCadastrarFuncionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button3 = new System.Windows.Forms.Button();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtsenha = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dateNascimento = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dateAdmissao = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.cboSexo = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.cbkProduto = new System.Windows.Forms.CheckBox();
            this.cbkCliente = new System.Windows.Forms.CheckBox();
            this.cbkPedido = new System.Windows.Forms.CheckBox();
            this.cbkEstoque = new System.Windows.Forms.CheckBox();
            this.cbkFuncionario = new System.Windows.Forms.CheckBox();
            this.cbkCaixa = new System.Windows.Forms.CheckBox();
            this.cbkFornecedor = new System.Windows.Forms.CheckBox();
            this.cbkPagamento = new System.Windows.Forms.CheckBox();
            this.txtCep = new System.Windows.Forms.MaskedTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Gold;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.Black;
            this.button3.Location = new System.Drawing.Point(12, 521);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(874, 36);
            this.button3.TabIndex = 33;
            this.button3.Text = "Cadastrar";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtemail
            // 
            this.txtemail.BackColor = System.Drawing.Color.Black;
            this.txtemail.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemail.ForeColor = System.Drawing.Color.White;
            this.txtemail.Location = new System.Drawing.Point(100, 118);
            this.txtemail.MaxLength = 45;
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(306, 23);
            this.txtemail.TabIndex = 1;
            this.txtemail.Text = "Crie um E-mail";
            this.txtemail.Click += new System.EventHandler(this.txtemail_Click);
            this.txtemail.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtemail_KeyPress);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(10, 116);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 22);
            this.label11.TabIndex = 29;
            this.label11.Text = "Email:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Poor Richard", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(276, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(362, 44);
            this.label8.TabIndex = 28;
            this.label8.Text = "Cadastrar Funcionários";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(10, 159);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 22);
            this.label14.TabIndex = 31;
            this.label14.Text = "Senha:";
            // 
            // txtsenha
            // 
            this.txtsenha.BackColor = System.Drawing.Color.Black;
            this.txtsenha.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsenha.ForeColor = System.Drawing.Color.White;
            this.txtsenha.Location = new System.Drawing.Point(100, 159);
            this.txtsenha.MaxLength = 20;
            this.txtsenha.Name = "txtsenha";
            this.txtsenha.Size = new System.Drawing.Size(306, 26);
            this.txtsenha.TabIndex = 2;
            this.txtsenha.Text = "qualquer";
            this.txtsenha.UseSystemPasswordChar = true;
            this.txtsenha.Click += new System.EventHandler(this.txtsenha_Click);
            this.txtsenha.TextChanged += new System.EventHandler(this.txtsenha_TextChanged);
            this.txtsenha.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtsenha_KeyPress);
            // 
            // txtNome
            // 
            this.txtNome.BackColor = System.Drawing.Color.Black;
            this.txtNome.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.ForeColor = System.Drawing.Color.White;
            this.txtNome.Location = new System.Drawing.Point(100, 207);
            this.txtNome.MaxLength = 50;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(306, 23);
            this.txtNome.TabIndex = 3;
            this.txtNome.Text = "Nome Completo";
            this.txtNome.Click += new System.EventHandler(this.txtNome_Click);
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(10, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 22);
            this.label2.TabIndex = 42;
            this.label2.Text = "Nome:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(242, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 22);
            this.label3.TabIndex = 44;
            this.label3.Text = "CPF:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(14, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 22);
            this.label4.TabIndex = 46;
            this.label4.Text = "RG:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(10, 409);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 22);
            this.label5.TabIndex = 48;
            this.label5.Text = "Nascimento:";
            // 
            // dateNascimento
            // 
            this.dateNascimento.Location = new System.Drawing.Point(140, 410);
            this.dateNascimento.Name = "dateNascimento";
            this.dateNascimento.Size = new System.Drawing.Size(266, 20);
            this.dateNascimento.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(10, 361);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 22);
            this.label6.TabIndex = 50;
            this.label6.Text = "Cargo:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(431, 115);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 22);
            this.label7.TabIndex = 52;
            this.label7.Text = "Telefone Fixo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(694, 115);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 22);
            this.label9.TabIndex = 54;
            this.label9.Text = "Celular:";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtRua
            // 
            this.txtRua.BackColor = System.Drawing.Color.Black;
            this.txtRua.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRua.ForeColor = System.Drawing.Color.White;
            this.txtRua.Location = new System.Drawing.Point(580, 204);
            this.txtRua.MaxLength = 45;
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(306, 23);
            this.txtRua.TabIndex = 12;
            this.txtRua.Text = "Nome da rua";
            this.txtRua.Click += new System.EventHandler(this.txtRua_Click);
            this.txtRua.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            this.txtRua.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRua_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(431, 201);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 22);
            this.label10.TabIndex = 56;
            this.label10.Text = "Rua:";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // txtBairro
            // 
            this.txtBairro.BackColor = System.Drawing.Color.Black;
            this.txtBairro.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBairro.ForeColor = System.Drawing.Color.White;
            this.txtBairro.Location = new System.Drawing.Point(580, 245);
            this.txtBairro.MaxLength = 45;
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(306, 23);
            this.txtBairro.TabIndex = 13;
            this.txtBairro.Text = "Bairro que mora";
            this.txtBairro.Click += new System.EventHandler(this.txtBairro_Click);
            this.txtBairro.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            this.txtBairro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBairro_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(431, 246);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(66, 22);
            this.label12.TabIndex = 58;
            this.label12.Text = "Bairro:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // txtNumero
            // 
            this.txtNumero.BackColor = System.Drawing.Color.Black;
            this.txtNumero.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.ForeColor = System.Drawing.Color.White;
            this.txtNumero.Location = new System.Drawing.Point(580, 289);
            this.txtNumero.MaxLength = 30;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(306, 23);
            this.txtNumero.TabIndex = 14;
            this.txtNumero.Text = "Nº da casa";
            this.txtNumero.Click += new System.EventHandler(this.txtNumero_Click);
            this.txtNumero.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(431, 291);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 22);
            this.label13.TabIndex = 60;
            this.label13.Text = "Nº";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(438, 454);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 22);
            this.label16.TabIndex = 66;
            this.label16.Text = "Admissão:";
            // 
            // dateAdmissao
            // 
            this.dateAdmissao.Location = new System.Drawing.Point(580, 456);
            this.dateAdmissao.Name = "dateAdmissao";
            this.dateAdmissao.Size = new System.Drawing.Size(306, 20);
            this.dateAdmissao.TabIndex = 15;
            this.dateAdmissao.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(10, 451);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(57, 22);
            this.label15.TabIndex = 68;
            this.label15.Text = "Sexo:";
            // 
            // cboSexo
            // 
            this.cboSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSexo.FormattingEnabled = true;
            this.cboSexo.Items.AddRange(new object[] {
            "Maculino",
            "Feminino"});
            this.cboSexo.Location = new System.Drawing.Point(140, 455);
            this.cboSexo.Name = "cboSexo";
            this.cboSexo.Size = new System.Drawing.Size(266, 21);
            this.cboSexo.TabIndex = 8;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(431, 163);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 22);
            this.label17.TabIndex = 70;
            this.label17.Text = "Cep:";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // cboCargo
            // 
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Items.AddRange(new object[] {
            "Administrador",
            "Atendente",
            "Bar"});
            this.cboCargo.Location = new System.Drawing.Point(140, 360);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(266, 21);
            this.cboCargo.TabIndex = 6;
            // 
            // txtRG
            // 
            this.txtRG.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtRG.ForeColor = System.Drawing.Color.White;
            this.txtRG.Location = new System.Drawing.Point(104, 253);
            this.txtRG.Mask = "99.999.999-9";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(73, 20);
            this.txtRG.TabIndex = 4;
            // 
            // txtCPF
            // 
            this.txtCPF.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtCPF.ForeColor = System.Drawing.Color.White;
            this.txtCPF.Location = new System.Drawing.Point(322, 250);
            this.txtCPF.Mask = "999999999/99";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(84, 20);
            this.txtCPF.TabIndex = 5;
            this.txtCPF.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCPF_MaskInputRejected);
            // 
            // txtTelefone
            // 
            this.txtTelefone.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtTelefone.ForeColor = System.Drawing.Color.White;
            this.txtTelefone.Location = new System.Drawing.Point(580, 121);
            this.txtTelefone.Mask = "(99) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(80, 20);
            this.txtTelefone.TabIndex = 9;
            this.txtTelefone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtTelefone_MaskInputRejected);
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtCelular.ForeColor = System.Drawing.Color.White;
            this.txtCelular.Location = new System.Drawing.Point(800, 121);
            this.txtCelular.Mask = "(99) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(86, 20);
            this.txtCelular.TabIndex = 10;
            this.txtCelular.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox4_MaskInputRejected);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(816, -37);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(25, 24);
            this.button5.TabIndex = 208;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Gold;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(855, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 24);
            this.label1.TabIndex = 207;
            this.label1.Text = "←";
            this.label1.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(614, 356);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(103, 22);
            this.label18.TabIndex = 209;
            this.label18.Text = "Permissão:";
            // 
            // cbkProduto
            // 
            this.cbkProduto.AutoSize = true;
            this.cbkProduto.Location = new System.Drawing.Point(442, 389);
            this.cbkProduto.Name = "cbkProduto";
            this.cbkProduto.Size = new System.Drawing.Size(63, 17);
            this.cbkProduto.TabIndex = 210;
            this.cbkProduto.Text = "Produto";
            this.cbkProduto.UseVisualStyleBackColor = true;
            // 
            // cbkCliente
            // 
            this.cbkCliente.AutoSize = true;
            this.cbkCliente.Location = new System.Drawing.Point(580, 389);
            this.cbkCliente.Name = "cbkCliente";
            this.cbkCliente.Size = new System.Drawing.Size(58, 17);
            this.cbkCliente.TabIndex = 211;
            this.cbkCliente.Text = "Cliente";
            this.cbkCliente.UseVisualStyleBackColor = true;
            // 
            // cbkPedido
            // 
            this.cbkPedido.AutoSize = true;
            this.cbkPedido.Location = new System.Drawing.Point(778, 389);
            this.cbkPedido.Name = "cbkPedido";
            this.cbkPedido.Size = new System.Drawing.Size(59, 17);
            this.cbkPedido.TabIndex = 212;
            this.cbkPedido.Text = "Pedido";
            this.cbkPedido.UseVisualStyleBackColor = true;
            // 
            // cbkEstoque
            // 
            this.cbkEstoque.AutoSize = true;
            this.cbkEstoque.Location = new System.Drawing.Point(679, 389);
            this.cbkEstoque.Name = "cbkEstoque";
            this.cbkEstoque.Size = new System.Drawing.Size(65, 17);
            this.cbkEstoque.TabIndex = 213;
            this.cbkEstoque.Text = "Estoque";
            this.cbkEstoque.UseVisualStyleBackColor = true;
            // 
            // cbkFuncionario
            // 
            this.cbkFuncionario.AutoSize = true;
            this.cbkFuncionario.Location = new System.Drawing.Point(679, 412);
            this.cbkFuncionario.Name = "cbkFuncionario";
            this.cbkFuncionario.Size = new System.Drawing.Size(81, 17);
            this.cbkFuncionario.TabIndex = 217;
            this.cbkFuncionario.Text = "Funcionario";
            this.cbkFuncionario.UseVisualStyleBackColor = true;
            this.cbkFuncionario.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // cbkCaixa
            // 
            this.cbkCaixa.AutoSize = true;
            this.cbkCaixa.Location = new System.Drawing.Point(778, 412);
            this.cbkCaixa.Name = "cbkCaixa";
            this.cbkCaixa.Size = new System.Drawing.Size(94, 17);
            this.cbkCaixa.TabIndex = 216;
            this.cbkCaixa.Text = "Fluxo de caixa";
            this.cbkCaixa.UseVisualStyleBackColor = true;
            this.cbkCaixa.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // cbkFornecedor
            // 
            this.cbkFornecedor.AutoSize = true;
            this.cbkFornecedor.Location = new System.Drawing.Point(580, 412);
            this.cbkFornecedor.Name = "cbkFornecedor";
            this.cbkFornecedor.Size = new System.Drawing.Size(80, 17);
            this.cbkFornecedor.TabIndex = 215;
            this.cbkFornecedor.Text = "Fornecedor";
            this.cbkFornecedor.UseVisualStyleBackColor = true;
            this.cbkFornecedor.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // cbkPagamento
            // 
            this.cbkPagamento.AutoSize = true;
            this.cbkPagamento.Location = new System.Drawing.Point(442, 412);
            this.cbkPagamento.Name = "cbkPagamento";
            this.cbkPagamento.Size = new System.Drawing.Size(123, 17);
            this.cbkPagamento.TabIndex = 214;
            this.cbkPagamento.Text = "Folha de pagamento";
            this.cbkPagamento.UseVisualStyleBackColor = true;
            this.cbkPagamento.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // txtCep
            // 
            this.txtCep.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.txtCep.ForeColor = System.Drawing.Color.White;
            this.txtCep.Location = new System.Drawing.Point(580, 165);
            this.txtCep.Mask = "99999-999";
            this.txtCep.Name = "txtCep";
            this.txtCep.Size = new System.Drawing.Size(58, 20);
            this.txtCep.TabIndex = 11;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Gold;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Century Gothic", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.Black;
            this.button2.Location = new System.Drawing.Point(658, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 22);
            this.button2.TabIndex = 220;
            this.button2.Text = "Buscar";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // frmCadastrarFuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(902, 569);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtCep);
            this.Controls.Add(this.cbkFuncionario);
            this.Controls.Add(this.cbkCaixa);
            this.Controls.Add(this.cbkFornecedor);
            this.Controls.Add(this.cbkPagamento);
            this.Controls.Add(this.cbkEstoque);
            this.Controls.Add(this.cbkPedido);
            this.Controls.Add(this.cbkCliente);
            this.Controls.Add(this.cbkProduto);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.txtRG);
            this.Controls.Add(this.cboCargo);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.cboSexo);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.dateAdmissao);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtBairro);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtRua);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dateNascimento);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtsenha);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmCadastrarFuncionario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "\'";
            this.Load += new System.EventHandler(this.frmCadastrarFuncionario_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtsenha;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dateNascimento;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dateAdmissao;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboSexo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtCelular;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.CheckBox cbkProduto;
        private System.Windows.Forms.CheckBox cbkCliente;
        private System.Windows.Forms.CheckBox cbkPedido;
        private System.Windows.Forms.CheckBox cbkEstoque;
        private System.Windows.Forms.CheckBox cbkFuncionario;
        private System.Windows.Forms.CheckBox cbkCaixa;
        private System.Windows.Forms.CheckBox cbkFornecedor;
        private System.Windows.Forms.CheckBox cbkPagamento;
        private System.Windows.Forms.MaskedTextBox txtCep;
        private System.Windows.Forms.Button button2;
    }
}