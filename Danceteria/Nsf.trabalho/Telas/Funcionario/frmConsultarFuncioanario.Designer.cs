﻿namespace Nsf.trabalho
{
    partial class frmConsultarFuncioanario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvFuncionario = new System.Windows.Forms.DataGridView();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.txtConsultarFuncionario = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.ID_Fornecedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_cnpj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_inscricao = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_rua = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_bairro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_local = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_cep = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_uf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_tel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_email = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewImageColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvFuncionario
            // 
            this.dgvFuncionario.AllowUserToAddRows = false;
            this.dgvFuncionario.AllowUserToDeleteRows = false;
            this.dgvFuncionario.BackgroundColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFuncionario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFuncionario.ColumnHeadersHeight = 40;
            this.dgvFuncionario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Fornecedor,
            this.nm_empresa,
            this.nu_cnpj,
            this.nu_inscricao,
            this.nm_rua,
            this.nm_bairro,
            this.nu_local,
            this.nu_cep,
            this.nm_estado,
            this.ds_uf,
            this.nu_tel,
            this.ds_email,
            this.nm_funcionario,
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgvFuncionario.Location = new System.Drawing.Point(12, 131);
            this.dgvFuncionario.Name = "dgvFuncionario";
            this.dgvFuncionario.ReadOnly = true;
            this.dgvFuncionario.RowHeadersVisible = false;
            this.dgvFuncionario.Size = new System.Drawing.Size(652, 307);
            this.dgvFuncionario.TabIndex = 59;
            this.dgvFuncionario.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFuncionario_CellClick);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(536, 80);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(34, 26);
            this.button5.TabIndex = 58;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(576, 80);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(88, 26);
            this.button4.TabIndex = 57;
            this.button4.Text = "Procurar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtConsultarFuncionario
            // 
            this.txtConsultarFuncionario.Location = new System.Drawing.Point(231, 83);
            this.txtConsultarFuncionario.Name = "txtConsultarFuncionario";
            this.txtConsultarFuncionario.Size = new System.Drawing.Size(299, 20);
            this.txtConsultarFuncionario.TabIndex = 56;
            this.txtConsultarFuncionario.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(8, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(217, 21);
            this.label15.TabIndex = 55;
            this.label15.Text = "Nome do Funcionario";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::Nsf.trabalho.Properties.Resources.x_1152114_960_720;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dataGridViewImageColumn1.Width = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Poor Richard", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(171, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(359, 44);
            this.label8.TabIndex = 60;
            this.label8.Text = "Consultar Funcionários";
            // 
            // ID_Fornecedor
            // 
            this.ID_Fornecedor.DataPropertyName = "IdFuncionario";
            this.ID_Fornecedor.HeaderText = "ID";
            this.ID_Fornecedor.MinimumWidth = 10;
            this.ID_Fornecedor.Name = "ID_Fornecedor";
            this.ID_Fornecedor.ReadOnly = true;
            this.ID_Fornecedor.Width = 150;
            // 
            // nm_empresa
            // 
            this.nm_empresa.DataPropertyName = "email";
            this.nm_empresa.HeaderText = "E-mail";
            this.nm_empresa.Name = "nm_empresa";
            this.nm_empresa.ReadOnly = true;
            // 
            // nu_cnpj
            // 
            this.nu_cnpj.DataPropertyName = "funcionario";
            this.nu_cnpj.HeaderText = "Funcionario";
            this.nu_cnpj.Name = "nu_cnpj";
            this.nu_cnpj.ReadOnly = true;
            // 
            // nu_inscricao
            // 
            this.nu_inscricao.DataPropertyName = "rg";
            this.nu_inscricao.HeaderText = "RG";
            this.nu_inscricao.Name = "nu_inscricao";
            this.nu_inscricao.ReadOnly = true;
            // 
            // nm_rua
            // 
            this.nm_rua.DataPropertyName = "cpf";
            this.nm_rua.HeaderText = "CPF";
            this.nm_rua.Name = "nm_rua";
            this.nm_rua.ReadOnly = true;
            // 
            // nm_bairro
            // 
            this.nm_bairro.DataPropertyName = "cargo";
            this.nm_bairro.HeaderText = "Cargo";
            this.nm_bairro.Name = "nm_bairro";
            this.nm_bairro.ReadOnly = true;
            // 
            // nu_local
            // 
            this.nu_local.DataPropertyName = "nascimento";
            this.nu_local.HeaderText = "Nascimento";
            this.nu_local.Name = "nu_local";
            this.nu_local.ReadOnly = true;
            // 
            // nu_cep
            // 
            this.nu_cep.DataPropertyName = "telefone";
            this.nu_cep.HeaderText = "Telefone";
            this.nu_cep.Name = "nu_cep";
            this.nu_cep.ReadOnly = true;
            // 
            // nm_estado
            // 
            this.nm_estado.DataPropertyName = "celular";
            this.nm_estado.HeaderText = "Celular";
            this.nm_estado.Name = "nm_estado";
            this.nm_estado.ReadOnly = true;
            // 
            // ds_uf
            // 
            this.ds_uf.DataPropertyName = "rua";
            this.ds_uf.HeaderText = "Rua";
            this.ds_uf.Name = "ds_uf";
            this.ds_uf.ReadOnly = true;
            // 
            // nu_tel
            // 
            this.nu_tel.DataPropertyName = "bairro";
            this.nu_tel.HeaderText = "Bairro";
            this.nu_tel.Name = "nu_tel";
            this.nu_tel.ReadOnly = true;
            // 
            // ds_email
            // 
            this.ds_email.DataPropertyName = "numero";
            this.ds_email.HeaderText = "Numero";
            this.ds_email.Name = "ds_email";
            this.ds_email.ReadOnly = true;
            // 
            // nm_funcionario
            // 
            this.nm_funcionario.DataPropertyName = "cep";
            this.nm_funcionario.HeaderText = "CEP";
            this.nm_funcionario.Name = "nm_funcionario";
            this.nm_funcionario.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "sexo";
            this.Column1.HeaderText = "Sexo";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "admissao";
            this.Column2.HeaderText = "Admissao";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.Image = global::Nsf.trabalho.Properties.Resources.x_1152114_960_720;
            this.Column3.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Column3.Width = 25;
            // 
            // frmConsultarFuncioanario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(681, 450);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.dgvFuncionario);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtConsultarFuncionario);
            this.Controls.Add(this.label15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarFuncioanario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmConsultarFuncioanario";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFuncionario)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvFuncionario;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtConsultarFuncionario;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Fornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_empresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_cnpj;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_inscricao;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_rua;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_bairro;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_local;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_cep;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_estado;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_uf;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_tel;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_email;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewImageColumn Column3;
    }
}