﻿using Nsf.trabalho.API.Correios;
using Nsf.trabalho.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Funcionario
{
    public partial class frmAlterarFuncionario : Form
    {
        public frmAlterarFuncionario()
        {
            InitializeComponent();
            CarregarCombo();
        }


        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboId.ValueMember = nameof(FuncionarioDTO.IdFuncionario);
            cboId.DisplayMember = nameof(FuncionarioDTO.IdFuncionario);
            cboId.DataSource = lista;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();

                dto.IdFuncionario = Convert.ToInt32(cboId.Text.Trim());
                dto.email = txtemail.Text.Trim();
                dto.senha = txtsenha.Text.Trim();
                dto.funcionario = txtNome.Text.Trim();
                dto.rg = txtRG.Text.Trim();
                dto.cpf = txtCPF.Text.Trim();
                dto.cargo = cboCargo.Text;
                dto.nascimento = Convert.ToDateTime(dateNascimento.Text);
                dto.telefone = txtTelefone.Text.Trim();
                dto.celular = txtCelular.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.numero = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.sexo = cboSexo.Text.Trim();
                dto.admissao = Convert.ToDateTime(dateAdmissao.Text);

                dto.PermissaoProduto = cbkProduto.Checked;
                dto.PermissaoCliente = cbkCliente.Checked;
                dto.PermissaoEstoque = cbkEstoque.Checked;
                dto.PermissaoPedido = cbkPedido.Checked;
                dto.PermissaoFolhaPagamento = cbkPagamento.Checked;
                dto.PermissaoFornecedor = cbkFornecedor.Checked;
                dto.PermissaoFuncionario = cbkFuncionario.Checked;
                dto.PermissaoFluxoCaixa = cbkCaixa.Checked;

                if (dto.email == string.Empty || dto.email == "Crie um E-mail")
                {
                    MessageBox.Show("Crie um e-mail para login.");
                    return;
                }

                if (dto.senha == string.Empty)
                {
                    MessageBox.Show("Crie uma senha para login.");
                    return;
                }

                if (dto.funcionario == string.Empty || dto.funcionario == "Nome Completo")
                {
                    MessageBox.Show("Coloque o nome do funcionario.");
                    return;
                }

                if (dto.rg == string.Empty || txtRG.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros da RG.");
                    return;
                }

                if (dto.cpf == string.Empty || txtCPF.Text.Length <= 11)
                {
                    MessageBox.Show("Digite os numeros do CPF.");
                    return;
                }

                if (dto.cargo == string.Empty)
                {
                    MessageBox.Show("Selecione o cargo.");
                    return;
                }

                if (dto.telefone == string.Empty || txtTelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Telefone é obrigatório.");
                    return;
                }

                if (dto.celular == string.Empty || txtCelular.Text.Length <= 14)
                {
                    MessageBox.Show("Celular é obrigatório.");
                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Nome da rua")
                {
                    MessageBox.Show("Rua é obrigatória.");
                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro que mora")
                {
                    MessageBox.Show("Bairro é obrigatório.");
                    return;
                }

                if (dto.numero == string.Empty || dto.numero == "Nº da casa")
                {
                    MessageBox.Show("Numero é obrigatório.");
                    return;
                }

                if (dto.cep == string.Empty || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite os numeros do CEP.");
                    return;
                }

                if (dto.sexo == string.Empty)
                {
                    MessageBox.Show("Selecione o sexo.");
                    return;
                }

                FuncionarioDataBase db = new FuncionarioDataBase();
                db.Alterar(dto);

                MessageBox.Show("Funcionario Alterado confira na busca.", "Blackout",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("É preciso informar o Id");
                return;
            }
        }

        private void frmAlterarFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse response = correio.Endereco(txtCep.Text);
                txtRua.Text = response.Logradouro;
                txtBairro.Text = response.Bairro;
            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void txtemail_Click(object sender, EventArgs e)
        {
            if (txtemail.Text == "Crie um E-mail")
            {
                txtemail.Text = string.Empty;
            }
        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "qualquer")
            {
                txtsenha.Text = string.Empty;
            }
        }

        private void txtNome_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "Nome Completo")
            {
                txtNome.Text = string.Empty;
            }
        }

        private void txtRua_Click(object sender, EventArgs e)
        {
            if (txtRua.Text == "Nome da rua")
            {
                txtRua.Text = string.Empty;
            }
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            if (txtBairro.Text == "Bairro que mora")
            {
                txtBairro.Text = string.Empty;
            }
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {
            if (txtNumero.Text == "Nº da casa")
            {
                txtNumero.Text = string.Empty;
            }
        }

        private void txtemail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }

        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            FuncionarioDTO Pro = cboId.SelectedItem as FuncionarioDTO;

            FuncionarioBusiness business = new FuncionarioBusiness();
            FuncionarioDTO prod = business.ListarPorId(Pro.IdFuncionario);

            txtemail.Text = prod.email.ToString();
            txtsenha.Text = prod.senha.ToString();
            txtNome.Text = prod.funcionario.ToString();
            txtRG.Text = prod.rg.ToString();
            txtCPF.Text = prod.cpf.ToString();
            cboCargo.Text = prod.cargo.ToString();
            dateNascimento.Text = prod.nascimento.ToString();
            txtTelefone.Text = prod.telefone.ToString();
            txtCelular.Text = prod.celular.ToString();
            txtRua.Text = prod.rua.ToString();
            txtBairro.Text = prod.bairro.ToString();
            txtNumero.Text = prod.numero.ToString();
            txtCep.Text = prod.cep.ToString();
            cboSexo.Text = prod.sexo.ToString();
            dateAdmissao.Text = prod.admissao.ToString();
            cbkProduto.Checked = prod.PermissaoProduto;
            cbkCliente.Checked = prod.PermissaoCliente;
            cbkEstoque.Checked = prod.PermissaoEstoque;
            cbkPedido.Checked = prod.PermissaoPedido;
            cbkPagamento.Checked = prod.PermissaoFolhaPagamento;
            cbkFornecedor.Checked = prod.PermissaoFornecedor;
            cbkFuncionario.Checked = prod.PermissaoFuncionario;
            cbkCaixa.Checked = prod.PermissaoFluxoCaixa;

        }
    }
}
