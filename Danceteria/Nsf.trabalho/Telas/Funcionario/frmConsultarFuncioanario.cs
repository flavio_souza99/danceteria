﻿using Nsf.trabalho.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarFuncioanario : Form
    {
        public frmConsultarFuncioanario()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Consultar(txtConsultarFuncionario.Text.Trim());

            dgvFuncionario.AutoGenerateColumns = false;
            dgvFuncionario.DataSource = lista;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void dgvFuncionario_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 15)
                {
                    FuncionarioDTO cat = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;

                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(cat.IdFuncionario);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um dos funcionarios que não pode ser removido");
                frmConsultarFuncioanario ir = new frmConsultarFuncioanario();
                this.Hide();
                ir.ShowDialog();
            }
        }
    }
}
