﻿using Nsf.trabalho.Fornecedor;
using Nsf.trabalho.Funcionario;
using Nsf.trabalho.Pedido_Compra;
using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmPedirFornecedor : Form
    {

        BindingList<FornecedorDTO> Fornecedor = new BindingList<FornecedorDTO>();
        BindingList<ProdutoDTO> Produto = new BindingList<ProdutoDTO>();

        public frmPedirFornecedor()
       
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();
        }

        void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.IdFornecedor);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.empresa);
            cboFornecedor.DataSource = lista;
        }

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        private void frmPedirFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO Fun = cboFornecedor.SelectedItem as FornecedorDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.FornecedorId = Fun.IdFornecedor;
                dto.ProdutoId = Pro.IdProduto;
                dto.DtPedido = Convert.ToDateTime(txtData.Text);
                dto.QtdProduto = Convert.ToInt32(txtQuantidade.Text);

                PedidoCompraBusiness business = new PedidoCompraBusiness();
                business.Salvar(dto);
                MessageBox.Show("Pedido ao fornecedor realizadocom sucesso.", "Blackout", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas");
                return;
            }
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO dto = cboFornecedor.SelectedItem as FornecedorDTO;
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
        }

        private void cboFornecedor_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            FornecedorDTO dto = cboFornecedor.SelectedItem as FornecedorDTO;
        }

        private void cboProduto_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            ProdutoDTO dto = cboProduto.SelectedItem as ProdutoDTO;
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }    
}
