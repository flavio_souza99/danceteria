﻿namespace Nsf.trabalho.Telas.Pedido
{
    partial class frmConsultarPedidoFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.dgvPedidoFornecedor = new System.Windows.Forms.DataGridView();
            this.ID_Pedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fm_pagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtd_produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.txtEmpresa = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidoFornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Poor Richard", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(481, 44);
            this.label1.TabIndex = 132;
            this.label1.Text = "Consultar Pedido ao fornecedor";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(419, 77);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(31, 26);
            this.button5.TabIndex = 131;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dgvPedidoFornecedor
            // 
            this.dgvPedidoFornecedor.AllowUserToAddRows = false;
            this.dgvPedidoFornecedor.AllowUserToDeleteRows = false;
            this.dgvPedidoFornecedor.BackgroundColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPedidoFornecedor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPedidoFornecedor.ColumnHeadersHeight = 40;
            this.dgvPedidoFornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Pedido,
            this.fm_pagamento,
            this.qtd_produto,
            this.dt_data,
            this.Column2,
            this.Column1});
            this.dgvPedidoFornecedor.Location = new System.Drawing.Point(12, 113);
            this.dgvPedidoFornecedor.Name = "dgvPedidoFornecedor";
            this.dgvPedidoFornecedor.RowHeadersVisible = false;
            this.dgvPedidoFornecedor.Size = new System.Drawing.Size(529, 325);
            this.dgvPedidoFornecedor.TabIndex = 130;
            this.dgvPedidoFornecedor.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPedidoFornecedor_CellClick);
            // 
            // ID_Pedido
            // 
            this.ID_Pedido.DataPropertyName = "Id";
            this.ID_Pedido.HeaderText = "ID";
            this.ID_Pedido.Name = "ID_Pedido";
            // 
            // fm_pagamento
            // 
            this.fm_pagamento.DataPropertyName = "Produto";
            this.fm_pagamento.HeaderText = "Produto";
            this.fm_pagamento.Name = "fm_pagamento";
            // 
            // qtd_produto
            // 
            this.qtd_produto.DataPropertyName = "QtdProduto";
            this.qtd_produto.HeaderText = "Quantidade";
            this.qtd_produto.Name = "qtd_produto";
            // 
            // dt_data
            // 
            this.dt_data.DataPropertyName = "Fornecedor";
            this.dt_data.HeaderText = "Empresa";
            this.dt_data.Name = "dt_data";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "DtPedido";
            this.Column2.HeaderText = "Data";
            this.Column2.Name = "Column2";
            // 
            // Column1
            // 
            this.Column1.HeaderText = "";
            this.Column1.Image = global::Nsf.trabalho.Properties.Resources.x_1152114_960_720;
            this.Column1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Column1.Name = "Column1";
            this.Column1.Width = 25;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepPink;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(456, 77);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 26);
            this.button4.TabIndex = 129;
            this.button4.Text = "Procurar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtEmpresa
            // 
            this.txtEmpresa.BackColor = System.Drawing.Color.Black;
            this.txtEmpresa.ForeColor = System.Drawing.Color.White;
            this.txtEmpresa.Location = new System.Drawing.Point(108, 83);
            this.txtEmpresa.Name = "txtEmpresa";
            this.txtEmpresa.Size = new System.Drawing.Size(305, 20);
            this.txtEmpresa.TabIndex = 128;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(8, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 21);
            this.label15.TabIndex = 127;
            this.label15.Text = "Empresa";
            // 
            // frmConsultarPedidoFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(554, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.dgvPedidoFornecedor);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtEmpresa);
            this.Controls.Add(this.label15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarPedidoFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmConsultarPedidoFornecedor";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidoFornecedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dgvPedidoFornecedor;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtEmpresa;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Pedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn fm_pagamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtd_produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewImageColumn Column1;
    }
}