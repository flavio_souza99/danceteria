﻿using Nsf.trabalho.Pedido_Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Pedido
{
    public partial class frmConsultarPedidoFornecedor : Form
    {
        public frmConsultarPedidoFornecedor()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PedidoCompraBusiness business = new PedidoCompraBusiness();
            List<PedidoCompraViewConsultar> lista = business.Consultar(txtEmpresa.Text);

            dgvPedidoFornecedor.AutoGenerateColumns = false;
            dgvPedidoFornecedor.DataSource = lista;
        }

        private void dgvPedidoFornecedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    PedidoCompraViewConsultar cat = dgvPedidoFornecedor.CurrentRow.DataBoundItem as PedidoCompraViewConsultar;

                    PedidoCompraBusiness business = new PedidoCompraBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse não pode ser removido agora");
                return;
            }
        }
    }
}
