﻿namespace Nsf.trabalho
{
    partial class frmConsultarPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button5 = new System.Windows.Forms.Button();
            this.dgvPedido = new System.Windows.Forms.DataGridView();
            this.ID_Pedido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ds_produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.qtd_produto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_pago = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fm_pagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.txtConsultarVenda = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedido)).BeginInit();
            this.SuspendLayout();
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(611, 78);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(31, 26);
            this.button5.TabIndex = 49;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dgvPedido
            // 
            this.dgvPedido.AllowUserToAddRows = false;
            this.dgvPedido.AllowUserToDeleteRows = false;
            this.dgvPedido.BackgroundColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPedido.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPedido.ColumnHeadersHeight = 40;
            this.dgvPedido.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ID_Pedido,
            this.nm_nome,
            this.ds_produto,
            this.qtd_produto,
            this.vl_pago,
            this.Column2,
            this.fm_pagamento,
            this.dt_data,
            this.nm_funcionario,
            this.Column1,
            this.Column3});
            this.dgvPedido.Location = new System.Drawing.Point(22, 115);
            this.dgvPedido.Name = "dgvPedido";
            this.dgvPedido.ReadOnly = true;
            this.dgvPedido.RowHeadersVisible = false;
            this.dgvPedido.Size = new System.Drawing.Size(711, 325);
            this.dgvPedido.TabIndex = 48;
            this.dgvPedido.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPedido_CellClick);
            // 
            // ID_Pedido
            // 
            this.ID_Pedido.DataPropertyName = "Id";
            this.ID_Pedido.HeaderText = "ID";
            this.ID_Pedido.Name = "ID_Pedido";
            this.ID_Pedido.ReadOnly = true;
            // 
            // nm_nome
            // 
            this.nm_nome.DataPropertyName = "Cliente";
            this.nm_nome.HeaderText = "Cliente";
            this.nm_nome.Name = "nm_nome";
            this.nm_nome.ReadOnly = true;
            this.nm_nome.Width = 150;
            // 
            // ds_produto
            // 
            this.ds_produto.DataPropertyName = "Produto";
            this.ds_produto.HeaderText = "Produto";
            this.ds_produto.Name = "ds_produto";
            this.ds_produto.ReadOnly = true;
            this.ds_produto.Width = 150;
            // 
            // qtd_produto
            // 
            this.qtd_produto.DataPropertyName = "quantidade";
            this.qtd_produto.HeaderText = "Quantidade Produto";
            this.qtd_produto.Name = "qtd_produto";
            this.qtd_produto.ReadOnly = true;
            this.qtd_produto.Width = 150;
            // 
            // vl_pago
            // 
            this.vl_pago.DataPropertyName = "valorfinal";
            this.vl_pago.HeaderText = "Valor unitário";
            this.vl_pago.Name = "vl_pago";
            this.vl_pago.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "valortotal";
            this.Column2.HeaderText = "Valor total";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // fm_pagamento
            // 
            this.fm_pagamento.DataPropertyName = "pagamento";
            this.fm_pagamento.HeaderText = "Forma de Pagamento";
            this.fm_pagamento.Name = "fm_pagamento";
            this.fm_pagamento.ReadOnly = true;
            this.fm_pagamento.Width = 160;
            // 
            // dt_data
            // 
            this.dt_data.DataPropertyName = "data";
            this.dt_data.HeaderText = "Data";
            this.dt_data.Name = "dt_data";
            this.dt_data.ReadOnly = true;
            // 
            // nm_funcionario
            // 
            this.nm_funcionario.DataPropertyName = "Funcionario";
            this.nm_funcionario.HeaderText = "Funcionario";
            this.nm_funcionario.Name = "nm_funcionario";
            this.nm_funcionario.ReadOnly = true;
            this.nm_funcionario.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "vip";
            this.Column1.HeaderText = "Vip";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "";
            this.Column3.Image = global::Nsf.trabalho.Properties.Resources.x_1152114_960_720;
            this.Column3.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 25;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepPink;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(648, 78);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 26);
            this.button4.TabIndex = 47;
            this.button4.Text = "Procurar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtConsultarVenda
            // 
            this.txtConsultarVenda.BackColor = System.Drawing.Color.Black;
            this.txtConsultarVenda.ForeColor = System.Drawing.Color.White;
            this.txtConsultarVenda.Location = new System.Drawing.Point(254, 78);
            this.txtConsultarVenda.Name = "txtConsultarVenda";
            this.txtConsultarVenda.Size = new System.Drawing.Size(333, 20);
            this.txtConsultarVenda.TabIndex = 46;
            this.txtConsultarVenda.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(31, 75);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(217, 21);
            this.label15.TabIndex = 45;
            this.label15.Text = "Consultar por cliente";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Poor Richard", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(246, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 44);
            this.label1.TabIndex = 126;
            this.label1.Text = "Consultar Pedido";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmConsultarPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(760, 460);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.dgvPedido);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtConsultarVenda);
            this.Controls.Add(this.label15);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmConsultarPedido";
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedido)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dgvPedido;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtConsultarVenda;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Pedido;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_nome;
        private System.Windows.Forms.DataGridViewTextBoxColumn ds_produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn qtd_produto;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_pago;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn fm_pagamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_data;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewImageColumn Column3;
    }
}