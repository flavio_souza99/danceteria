﻿using Nsf.trabalho.Fornecedor;
using Nsf.trabalho.Pedido_Compra;
using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Pedido
{
    public partial class frmAlterarPedidoFornecedor : Form
    {
        BindingList<FornecedorDTO> Fornecedor = new BindingList<FornecedorDTO>();
        BindingList<ProdutoDTO> Produto = new BindingList<ProdutoDTO>();

        public frmAlterarPedidoFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();
            CarregarCombo5();
        }

        void CarregarCombo5()
        {
            PedidoCompraBusiness business = new PedidoCompraBusiness();
            List<PedidoCompraDTO> lista = business.Listar();

            cboId.ValueMember = nameof(PedidoCompraDTO.Id);
            cboId.DisplayMember = nameof(PedidoCompraDTO.Id);
            cboId.DataSource = lista;
        }

        void CarregarCombo()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.IdFornecedor);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.empresa);
            cboFornecedor.DataSource = lista;
        }

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO Fun = cboFornecedor.SelectedItem as FornecedorDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.Id = Convert.ToInt32(cboId.Text);
                dto.FornecedorId = Fun.IdFornecedor;
                dto.ProdutoId = Pro.IdProduto;
                dto.DtPedido = Convert.ToDateTime(txtData.Text);
                dto.QtdProduto = Convert.ToInt32(txtQuantidade.Text);

                PedidoCompraBusiness business = new PedidoCompraBusiness();
                business.Alterar(dto);
                MessageBox.Show("Pedido ao fornecedor alterado com sucesso.", "Blackout", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Informações invalidas");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            PedidoCompraDTO Pro = cboId.SelectedItem as PedidoCompraDTO;

            PedidoCompraBusiness business = new PedidoCompraBusiness();
            PedidoCompraDTO prod = business.ListarPorId(Pro.Id);


            txtData.Text = prod.DtPedido.ToString();
            txtQuantidade.Text = prod.QtdProduto.ToString();
            

         
        }
    }        
}
