﻿using MySql.Data.MySqlClient;
using Nsf.trabalho.BancoDeDados;
using Nsf.trabalho.Cliente;
using Nsf.trabalho.Funcionario;
using Nsf.trabalho.Pedido_Venda;
using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmCadastrarPedido : Form
    {
        public frmCadastrarPedido()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();
            CarregarComboss();
        }
        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista1 = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.IdFuncionario);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista1;
        }

        void CarregarCombos()
        {
            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProdutoDTO> lista2 = business2.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista2;       
        }

        void CarregarComboss()
        {
            ClienteBusiness business3 = new ClienteBusiness();
            List<ClienteDTO> lista3 = business3.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.IdCliente);
            cboCliente.DisplayMember = nameof(ClienteDTO.nome);
            cboCliente.DataSource = lista3;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            try
            {

                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;
                ClienteDTO Cli = cboCliente.SelectedItem as ClienteDTO;

                PedidoVendaDTO dto = new PedidoVendaDTO();
                dto.ClienteId = Cli.IdCliente;
                dto.FuncionarioId = Fun.IdFuncionario;
                dto.ProdutoId = Pro.IdProduto;
                dto.quantidade = Convert.ToInt32(txtQuantidade.Text);
                dto.pagamento = txtPagamento.Text;
                dto.data = Convert.ToDateTime(txtData.Text);
                dto.valorfinal = Convert.ToDecimal(lblPreco.Text);
                dto.vip = txtVip.Text;
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                lblTotal.Text = Convert.ToString((Convert.ToInt32(txtQuantidade.Text)) * (Convert.ToDecimal(lblPreco.Text)));
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                if (txtVip.Text == "Sim")
                {
                    lblTotal.Text = Convert.ToString((Convert.ToInt32(txtQuantidade.Text)) * (Convert.ToDecimal(lblPreco.Text)) + 100);
                    dto.valortotal = Convert.ToDecimal(lblTotal.Text);
                }


                if (dto.pagamento == string.Empty)
                {
                    MessageBox.Show("Selecione a forma de pagamento");
                    return;
                }

                if (dto.vip == string.Empty)
                {
                    MessageBox.Show("Selecione se deseja conteudo vip ou não");
                    return;
                }


                PedidoVendaBusiness business = new PedidoVendaBusiness();
                business.Salvar(dto);
                MessageBox.Show("Pedido realizado com sucesso.", "Danceteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas");
                return;
            }
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            lblPreco.Text = prod.valorproduto.ToString();
        }

        private void txtQuantidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void cboCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }
    }
}
