﻿using Nsf.trabalho.Cliente;
using Nsf.trabalho.Funcionario;
using Nsf.trabalho.Pedido_Compra;
using Nsf.trabalho.Pedido_Venda;
using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Pedido
{
    public partial class frmAlterarPedido : Form
    {
        public frmAlterarPedido()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();
            CarregarComboss();
            CarregarCombo5();
        }

        void CarregarCombo5()
        {
            PedidoVendaBusiness business = new PedidoVendaBusiness();
            List<PedidoVendaDTO> lista = business.Listar();

            cboId.ValueMember = nameof(PedidoVendaDTO.Id);
            cboId.DisplayMember = nameof(PedidoVendaDTO.Id);
            cboId.DataSource = lista;
        }


        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista1 = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.IdFuncionario);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista1;
        }

        void CarregarCombos()
        {
            ProdutoBusiness business2 = new ProdutoBusiness();
            List<ProdutoDTO> lista2 = business2.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista2;
        }

        void CarregarComboss()
        {
            ClienteBusiness business3 = new ClienteBusiness();
            List<ClienteDTO> lista3 = business3.Listar();

            cboCliente.ValueMember = nameof(ClienteDTO.IdCliente);
            cboCliente.DisplayMember = nameof(ClienteDTO.nome);
            cboCliente.DataSource = lista3;
        }


        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;
                ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;
                ClienteDTO Cli = cboCliente.SelectedItem as ClienteDTO;

                PedidoVendaDTO dto = new PedidoVendaDTO();
                dto.Id = Convert.ToInt32(cboId.Text);
                dto.ClienteId = Cli.IdCliente;
                dto.FuncionarioId = Fun.IdFuncionario;
                dto.ProdutoId = Pro.IdProduto;
                dto.quantidade = Convert.ToInt32(txtQuantidade.Text);
                dto.pagamento = txtPagamento.Text;
                dto.data = Convert.ToDateTime(txtData.Text);
                dto.valorfinal = Convert.ToDecimal(lblPreco.Text);
                dto.vip = txtVip.Text;
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                lblTotal.Text = Convert.ToString((Convert.ToInt32(txtQuantidade.Text)) * (Convert.ToDecimal(lblPreco.Text)));
                dto.valortotal = Convert.ToDecimal(lblTotal.Text);

                if (txtVip.Text == "Sim")
                {
                    lblTotal.Text = Convert.ToString((Convert.ToInt32(txtQuantidade.Text)) * (Convert.ToDecimal(lblPreco.Text)) + 100);
                    dto.valortotal = Convert.ToDecimal(lblTotal.Text);
                }


                if (dto.pagamento == string.Empty)
                {
                    MessageBox.Show("Selecione a forma de pagamento");
                    return;
                }

                if (dto.vip == string.Empty)
                {
                    MessageBox.Show("Selecione se deseja conteudo vip ou não");
                    return;
                }

                PedidoVendaBusiness business = new PedidoVendaBusiness();
                business.Alterar(dto);
                MessageBox.Show("Pedido alterado com sucesso.", "Danceteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas");
                return;
            }
        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            lblPreco.Text = prod.valorproduto.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            PedidoVendaDTO Pro = cboId.SelectedItem as PedidoVendaDTO;

            PedidoVendaBusiness business = new PedidoVendaBusiness();
            PedidoVendaDTO prod = business.ListarPorId(Pro.Id);

            txtQuantidade.Text = prod.quantidade.ToString();
            txtPagamento.Text = prod.pagamento.ToString();
            txtData.Text = prod.data.ToString();
            lblPreco.Text = prod.valorfinal.ToString();
            txtVip.Text = prod.vip.ToString();
            lblTotal.Text = prod.valortotal.ToString();     

        }

        private void frmAlterarPedido_Load(object sender, EventArgs e)
        {

        }
    }
}
