﻿using Nsf.trabalho.Pedido_Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarPedido : Form
    {
        public frmConsultarPedido()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            PedidoVendaBusiness business = new PedidoVendaBusiness();
            List<PedidoVendaConsultarView> lista = business.Consultar(txtConsultarVenda.Text.Trim());

            dgvPedido.AutoGenerateColumns = false;
            dgvPedido.DataSource = lista;
        }

        private void dgvPedido_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 10)
                {
                    PedidoVendaConsultarView cat = dgvPedido.CurrentRow.DataBoundItem as PedidoVendaConsultarView;

                    PedidoVendaBusiness business = new PedidoVendaBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse é um pedido não pode ser removido");
                return;
            }
        }
    }
}
