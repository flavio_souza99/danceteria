﻿using Nsf.trabalho.API.Correios;
using Nsf.trabalho.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Cliente
{
    public partial class frmAlterarCliente : Form
    {
        public frmAlterarCliente()
        {
            InitializeComponent();
            CarregarCombo();
        }

        void CarregarCombo()
        {
            ClienteBusiness business = new ClienteBusiness();
            List<ClienteDTO> lista = business.Listar();

            cboId.ValueMember = nameof(ClienteDTO.IdCliente);
            cboId.DisplayMember = nameof(ClienteDTO.IdCliente);
            cboId.DataSource = lista;
        }


        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO dto = new ClienteDTO();

                dto.IdCliente = Convert.ToInt32(cboId.Text.Trim());
                dto.nome = txtnome.Text.Trim();
                dto.rg = txtrg1.Text.Trim();
                dto.cpf = txtcpf.Text.Trim();
                dto.sexo = cbosexo.Text.Trim();
                dto.nascimento = Convert.ToDateTime(txtnascimento.Text);
                dto.telefone = txttelefone.Text.Trim();
                dto.celular = txtcelular.Text.Trim();
                dto.rua = txtRua1.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.numero = txtnumero.Text.Trim();




                if (dto.nome == string.Empty || dto.nome == "Digite seu nome")
                {
                    MessageBox.Show("Digite seu nome");
                    return;
                }

                if (dto.rg == string.Empty || txtrg1.Text.Length <= 11)
                {
                    MessageBox.Show("Digite seu RG!");
                    return;
                }

                if (dto.cpf == string.Empty || txtcpf.Text.Length <= 11)
                {
                    MessageBox.Show("Coloque seu CPF.");
                    return;
                }


                if (dto.sexo == string.Empty)
                {
                    MessageBox.Show("Selecione seu sexo.");
                    return;
                }
                if (dto.telefone == string.Empty || txttelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Telefone é obrigatório.");
                    return;
                }


                if (dto.celular == string.Empty || txtcelular.Text.Length <= 14)
                {
                    MessageBox.Show("Celular é obrigatório.");
                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Nome da rua")
                {
                    MessageBox.Show("Rua é obrigatória.");
                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro que mora")
                {
                    MessageBox.Show("Bairro é obrigatório.");
                    return;
                }
                if (dto.cep == string.Empty || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite os numeros do CEP.");
                    return;
                }

                if (dto.numero == string.Empty || dto.numero == "Nº da casa")
                {
                    MessageBox.Show("Numero é obrigatório.");
                    return;
                }

                ClienteBusiness business = new ClienteBusiness();
                business.Alterar(dto);

                MessageBox.Show("Cliente Alterado.", "Blackout",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmMenu ir = new frmMenu();
                this.Hide();
                ir.ShowDialog();
            }
            catch (Exception)
            {

                MessageBox.Show("Dados Invalidos");
                return;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua1.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtnumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtRua1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtnome_Click(object sender, EventArgs e)
        {
            if (txtnome.Text == "Nome Completo")
            {
                txtnome.Text = string.Empty;
            }
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            if (txtBairro.Text == "Bairro que mora")
            {
                txtBairro.Text = string.Empty;
            }
        }

        private void txtnumero_Click(object sender, EventArgs e)
        {
            if (txtnumero.Text == "Nº da casa")
            {
                txtnumero.Text = string.Empty;
            }
        }

        private void txtRua1_Click(object sender, EventArgs e)
        {
            if (txtRua1.Text == "Nome da rua")
            {
                txtRua1.Text = string.Empty;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClienteDTO Pro = cboId.SelectedItem as ClienteDTO;

            ClienteBusiness business = new ClienteBusiness();
            ClienteDTO prod = business.ListarPorId(Pro.IdCliente);

            txtnome.Text = prod.nome.ToString();
            txtrg1.Text = prod.rg.ToString();
            txtcpf.Text = prod.cpf.ToString();
            cbosexo.Text = prod.sexo.ToString();
            txtnascimento.Text = prod.nascimento.ToString();
            txttelefone.Text = prod.telefone.ToString();
            txtcelular.Text = prod.celular.ToString();
            txtRua1.Text = prod.rua.ToString();
            txtBairro.Text = prod.bairro.ToString();
            txtCep.Text = prod.cep.ToString();
            txtnumero.Text = prod.numero.ToString();
        }

        private void frmAlterarCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
