﻿namespace Nsf.trabalho.Telas.Folha_de_pagamento
{
    partial class frmAlterarFolhaPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtTransporte = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRefeicao = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtHr2 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtHr1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAliquota = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSalarioFinal = new System.Windows.Forms.TextBox();
            this.txtInss = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSalarioHora = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHoraMes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cboFuncionario = new System.Windows.Forms.ComboBox();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboId = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(397, 340);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(161, 22);
            this.label19.TabIndex = 280;
            this.label19.Text = "Valores por fora:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(738, 252);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 22);
            this.label18.TabIndex = 279;
            this.label18.Text = "- INSS";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(690, 413);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 22);
            this.label17.TabIndex = 278;
            this.label17.Text = "Vl transporte:";
            // 
            // txtTransporte
            // 
            this.txtTransporte.BackColor = System.Drawing.Color.Black;
            this.txtTransporte.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTransporte.ForeColor = System.Drawing.Color.White;
            this.txtTransporte.Location = new System.Drawing.Point(829, 413);
            this.txtTransporte.Name = "txtTransporte";
            this.txtTransporte.Size = new System.Drawing.Size(78, 23);
            this.txtTransporte.TabIndex = 277;
            this.txtTransporte.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTransporte_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(486, 412);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(114, 22);
            this.label16.TabIndex = 276;
            this.label16.Text = "Vl refeição:";
            // 
            // txtRefeicao
            // 
            this.txtRefeicao.BackColor = System.Drawing.Color.Black;
            this.txtRefeicao.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefeicao.ForeColor = System.Drawing.Color.White;
            this.txtRefeicao.Location = new System.Drawing.Point(606, 411);
            this.txtRefeicao.Name = "txtRefeicao";
            this.txtRefeicao.Size = new System.Drawing.Size(78, 23);
            this.txtRefeicao.TabIndex = 275;
            this.txtRefeicao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRefeicao_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(247, 412);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 22);
            this.label15.TabIndex = 274;
            this.label15.Text = "Hr extra 100%";
            // 
            // txtHr2
            // 
            this.txtHr2.BackColor = System.Drawing.Color.Black;
            this.txtHr2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHr2.ForeColor = System.Drawing.Color.White;
            this.txtHr2.Location = new System.Drawing.Point(387, 412);
            this.txtHr2.Name = "txtHr2";
            this.txtHr2.Size = new System.Drawing.Size(78, 23);
            this.txtHr2.TabIndex = 273;
            this.txtHr2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHr2_KeyPress);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(24, 412);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 22);
            this.label14.TabIndex = 272;
            this.label14.Text = "Hr extra 50%";
            // 
            // txtHr1
            // 
            this.txtHr1.BackColor = System.Drawing.Color.Black;
            this.txtHr1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHr1.ForeColor = System.Drawing.Color.White;
            this.txtHr1.Location = new System.Drawing.Point(153, 412);
            this.txtHr1.Name = "txtHr1";
            this.txtHr1.Size = new System.Drawing.Size(78, 23);
            this.txtHr1.TabIndex = 271;
            this.txtHr1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHr1_KeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(316, 251);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 22);
            this.label13.TabIndex = 270;
            this.label13.Text = "Sálario Final:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(884, 210);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(22, 22);
            this.label12.TabIndex = 269;
            this.label12.Text = "=";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(687, 208);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 22);
            this.label11.TabIndex = 268;
            this.label11.Text = "Aliquota:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(662, 209);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 22);
            this.label10.TabIndex = 267;
            this.label10.Text = "x";
            // 
            // txtAliquota
            // 
            this.txtAliquota.BackColor = System.Drawing.Color.Black;
            this.txtAliquota.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAliquota.ForeColor = System.Drawing.Color.White;
            this.txtAliquota.Location = new System.Drawing.Point(786, 209);
            this.txtAliquota.Name = "txtAliquota";
            this.txtAliquota.Size = new System.Drawing.Size(78, 23);
            this.txtAliquota.TabIndex = 266;
            this.txtAliquota.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAliquota_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(494, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 22);
            this.label4.TabIndex = 265;
            this.label4.Text = "Valor:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(26, 251);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 22);
            this.label3.TabIndex = 264;
            this.label3.Text = "INSS:";
            // 
            // txtSalarioFinal
            // 
            this.txtSalarioFinal.BackColor = System.Drawing.Color.White;
            this.txtSalarioFinal.Enabled = false;
            this.txtSalarioFinal.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalarioFinal.ForeColor = System.Drawing.Color.Black;
            this.txtSalarioFinal.Location = new System.Drawing.Point(470, 251);
            this.txtSalarioFinal.Name = "txtSalarioFinal";
            this.txtSalarioFinal.Size = new System.Drawing.Size(248, 23);
            this.txtSalarioFinal.TabIndex = 263;
            // 
            // txtInss
            // 
            this.txtInss.BackColor = System.Drawing.Color.White;
            this.txtInss.Enabled = false;
            this.txtInss.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInss.ForeColor = System.Drawing.Color.Black;
            this.txtInss.Location = new System.Drawing.Point(142, 251);
            this.txtInss.Name = "txtInss";
            this.txtInss.Size = new System.Drawing.Size(78, 23);
            this.txtInss.TabIndex = 262;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(235, 201);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 22);
            this.label9.TabIndex = 261;
            this.label9.Text = "x";
            // 
            // txtSalarioHora
            // 
            this.txtSalarioHora.BackColor = System.Drawing.Color.Black;
            this.txtSalarioHora.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSalarioHora.ForeColor = System.Drawing.Color.White;
            this.txtSalarioHora.Location = new System.Drawing.Point(364, 207);
            this.txtSalarioHora.Name = "txtSalarioHora";
            this.txtSalarioHora.Size = new System.Drawing.Size(87, 23);
            this.txtSalarioHora.TabIndex = 260;
            this.txtSalarioHora.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSalarioHora_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(260, 203);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(98, 22);
            this.label7.TabIndex = 259;
            this.label7.Text = "Sálario Hr:";
            // 
            // txtHoraMes
            // 
            this.txtHoraMes.BackColor = System.Drawing.Color.Black;
            this.txtHoraMes.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHoraMes.ForeColor = System.Drawing.Color.White;
            this.txtHoraMes.Location = new System.Drawing.Point(142, 203);
            this.txtHoraMes.Name = "txtHoraMes";
            this.txtHoraMes.Size = new System.Drawing.Size(78, 23);
            this.txtHoraMes.TabIndex = 258;
            this.txtHoraMes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtHoraMes_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(26, 201);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 22);
            this.label5.TabIndex = 257;
            this.label5.Text = "Horas/Mês:";
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.MediumSeaGreen;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(30, 483);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(879, 36);
            this.button4.TabIndex = 256;
            this.button4.Text = "Alterar erro de pagamento";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(515, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 22);
            this.label2.TabIndex = 255;
            this.label2.Text = "Data paga:";
            // 
            // cboFuncionario
            // 
            this.cboFuncionario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFuncionario.FormattingEnabled = true;
            this.cboFuncionario.Items.AddRange(new object[] {
            "Administrador",
            "Atendente",
            "Bar"});
            this.cboFuncionario.Location = new System.Drawing.Point(167, 103);
            this.cboFuncionario.Name = "cboFuncionario";
            this.cboFuncionario.Size = new System.Drawing.Size(266, 21);
            this.cboFuncionario.TabIndex = 254;
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.Color.White;
            this.txtValor.Enabled = false;
            this.txtValor.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtValor.ForeColor = System.Drawing.Color.Black;
            this.txtValor.Location = new System.Drawing.Point(564, 209);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(73, 23);
            this.txtValor.TabIndex = 253;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(466, 207);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(22, 22);
            this.label20.TabIndex = 252;
            this.label20.Text = "=";
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(640, 103);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(266, 20);
            this.txtData.TabIndex = 251;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(28, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 22);
            this.label6.TabIndex = 250;
            this.label6.Text = "Funcionário:";
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(893, 12);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(24, 24);
            this.button5.TabIndex = 249;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Poor Richard", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(294, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(387, 44);
            this.label8.TabIndex = 248;
            this.label8.Text = "Alterar folha pagamento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(28, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 22);
            this.label1.TabIndex = 282;
            this.label1.Text = "ID:";
            // 
            // cboId
            // 
            this.cboId.FormattingEnabled = true;
            this.cboId.Location = new System.Drawing.Point(80, 37);
            this.cboId.Name = "cboId";
            this.cboId.Size = new System.Drawing.Size(69, 21);
            this.cboId.TabIndex = 283;
            this.cboId.SelectedIndexChanged += new System.EventHandler(this.cboId_SelectedIndexChanged);
            // 
            // frmAlterarFolhaPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(942, 539);
            this.Controls.Add(this.cboId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.txtTransporte);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtRefeicao);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtHr2);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtHr1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtAliquota);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtSalarioFinal);
            this.Controls.Add(this.txtInss);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSalarioHora);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtHoraMes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboFuncionario);
            this.Controls.Add(this.txtValor);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label8);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmAlterarFolhaPagamento";
            this.Text = "frmAlterarFolhaPagamento";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtTransporte;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRefeicao;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtHr2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtHr1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtAliquota;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSalarioFinal;
        private System.Windows.Forms.TextBox txtInss;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSalarioHora;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtHoraMes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboFuncionario;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.DateTimePicker txtData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboId;
    }
}