﻿using Nsf.trabalho.Folha_de_Pagamento;
using Nsf.trabalho.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Folha_de_pagamento
{
    public partial class frmAlterarFolhaPagamento : Form
    {
        public frmAlterarFolhaPagamento()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();
        }


        void CarregarCombos()
        {
            PagamentoBusiness business2 = new PagamentoBusiness();
            List<PagamentoDTO> lista2 = business2.Listar();

            cboId.ValueMember = nameof(PagamentoDTO.Id);
            cboId.DisplayMember = nameof(PagamentoDTO.Id);
            cboId.DataSource = lista2;
        }

        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.IdFuncionario);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                PagamentoDTO dto = new PagamentoDTO();
                dto.Id = Convert.ToInt32(cboId.Text.Trim());
                dto.FuncionarioId = Fun.IdFuncionario;
                dto.data = Convert.ToDateTime(txtData.Text);
                dto.horames = txtHoraMes.Text;
                dto.salariohora = txtSalarioHora.Text;
                dto.valor = txtValor.Text;
                dto.aliquota = txtAliquota.Text;
                dto.inss = txtInss.Text;
                dto.salariofinal = txtSalarioFinal.Text;
                dto.horaext50 = txtHr1.Text;
                dto.horaext100 = txtHr2.Text;
                dto.vlrefeicao = txtRefeicao.Text;
                dto.vltransporte = txtTransporte.Text;

                if (txtHoraMes.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar a quantidades de horas trabalhadas!");
                    return;
                }

                if (txtSalarioHora.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar o valor do salário hora!");
                    return;
                }

                if (txtAliquota.Text == string.Empty)
                {
                    MessageBox.Show("É preciso informar a aliquota para calculo do inss!");
                    return;
                }

                txtValor.Text = Convert.ToString((Convert.ToDecimal(txtHoraMes.Text)) * (Convert.ToDecimal(txtSalarioHora.Text)));
                dto.valor = txtValor.Text;

                txtInss.Text = Convert.ToString((Convert.ToDecimal(txtValor.Text)) * ((Convert.ToDecimal(txtAliquota.Text)) / 100));
                dto.inss = txtInss.Text;

                txtSalarioFinal.Text = Convert.ToString((Convert.ToDecimal(txtValor.Text)) - (Convert.ToDecimal(txtInss.Text)));
                dto.salariofinal = txtSalarioFinal.Text;

                PagamentoDataBase db = new PagamentoDataBase();
                db.Alterar(dto);

                MessageBox.Show("Folha de pagamento Alterada com sucesso.", "Danceteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Informações invalidas.", "Danceteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void txtHoraMes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtSalarioHora_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtAliquota_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtHr1_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtHr2_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtRefeicao_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtTransporte_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            PagamentoDTO Pro = cboId.SelectedItem as PagamentoDTO;

            PagamentoBusiness business = new PagamentoBusiness();
            PagamentoDTO prod = business.ListarPorId(Pro.Id);

            txtData.Text = prod.data.ToString();
            txtHoraMes.Text = prod.horames.ToString();
            txtSalarioHora.Text = prod.salariohora.ToString();
            txtValor.Text = prod.valor.ToString();
            txtAliquota.Text = prod.aliquota.ToString();
            txtInss.Text = prod.inss.ToString();
            txtSalarioFinal.Text = prod.salariofinal.ToString();
            txtHr1.Text = prod.horaext50.ToString();
            txtHr2.Text = prod.horaext100.ToString();
            txtRefeicao.Text = prod.vlrefeicao.ToString();
            txtTransporte.Text = prod.vltransporte.ToString();

        }
    }
}
