﻿using Nsf.trabalho.Folha_de_Pagamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarFolhadePagamento : Form
    {
        public frmConsultarFolhadePagamento()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PagamentoBusiness business = new PagamentoBusiness();
            List<PagamentoConsultarView> lista = business.Consultar(txtFolhaPagamento.Text);

            dgvFolhadePagamento.AutoGenerateColumns = false;
            dgvFolhadePagamento.DataSource = lista;
        }

        private void dgvFolhadePagamento_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 9)
                {
                    PagamentoConsultarView cat = dgvFolhadePagamento.CurrentRow.DataBoundItem as PagamentoConsultarView;

                    PagamentoBusiness business = new PagamentoBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse não pode ser removido agora");
                frmConsultarFolhadePagamento ir = new frmConsultarFolhadePagamento();
                this.Hide();
                ir.ShowDialog();
            }
        }
    }
}
