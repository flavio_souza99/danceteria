﻿namespace Nsf.trabalho
{
    partial class frmConsultarFolhadePagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.button5 = new System.Windows.Forms.Button();
            this.dgvFolhadePagamento = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID_Folha_Pagamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_funcionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nu_horas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_salario_hora = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nm_empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vl_fornecedor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dt_paga = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.button4 = new System.Windows.Forms.Button();
            this.txtFolhaPagamento = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFolhadePagamento)).BeginInit();
            this.SuspendLayout();
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Gold;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.button5.ForeColor = System.Drawing.Color.Black;
            this.button5.Location = new System.Drawing.Point(692, 66);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(31, 26);
            this.button5.TabIndex = 49;
            this.button5.Text = "←";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dgvFolhadePagamento
            // 
            this.dgvFolhadePagamento.AllowUserToAddRows = false;
            this.dgvFolhadePagamento.AllowUserToDeleteRows = false;
            this.dgvFolhadePagamento.BackgroundColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFolhadePagamento.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFolhadePagamento.ColumnHeadersHeight = 40;
            this.dgvFolhadePagamento.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ID_Folha_Pagamento,
            this.nm_funcionario,
            this.nu_horas,
            this.vl_salario_hora,
            this.vl_total,
            this.nm_empresa,
            this.vl_fornecedor,
            this.dt_paga,
            this.Column2});
            this.dgvFolhadePagamento.Location = new System.Drawing.Point(12, 107);
            this.dgvFolhadePagamento.Name = "dgvFolhadePagamento";
            this.dgvFolhadePagamento.RowHeadersVisible = false;
            this.dgvFolhadePagamento.Size = new System.Drawing.Size(802, 356);
            this.dgvFolhadePagamento.TabIndex = 48;
            this.dgvFolhadePagamento.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFolhadePagamento_CellClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Id";
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            // 
            // ID_Folha_Pagamento
            // 
            this.ID_Folha_Pagamento.DataPropertyName = "Funcionario";
            this.ID_Folha_Pagamento.HeaderText = "Funcionario";
            this.ID_Folha_Pagamento.Name = "ID_Folha_Pagamento";
            this.ID_Folha_Pagamento.Width = 150;
            // 
            // nm_funcionario
            // 
            this.nm_funcionario.DataPropertyName = "data";
            this.nm_funcionario.HeaderText = "Data";
            this.nm_funcionario.Name = "nm_funcionario";
            // 
            // nu_horas
            // 
            this.nu_horas.DataPropertyName = "inss";
            this.nu_horas.HeaderText = "INSS";
            this.nu_horas.Name = "nu_horas";
            // 
            // vl_salario_hora
            // 
            this.vl_salario_hora.DataPropertyName = "salariofinal";
            this.vl_salario_hora.HeaderText = "Salario Final";
            this.vl_salario_hora.Name = "vl_salario_hora";
            // 
            // vl_total
            // 
            this.vl_total.DataPropertyName = "horaext50";
            this.vl_total.HeaderText = "Hora extra 50%";
            this.vl_total.Name = "vl_total";
            this.vl_total.Width = 120;
            // 
            // nm_empresa
            // 
            this.nm_empresa.DataPropertyName = "horaext100";
            this.nm_empresa.HeaderText = "Hora extra 100%";
            this.nm_empresa.Name = "nm_empresa";
            this.nm_empresa.Width = 130;
            // 
            // vl_fornecedor
            // 
            this.vl_fornecedor.DataPropertyName = "vlrefeicao";
            this.vl_fornecedor.HeaderText = "Vale refeição";
            this.vl_fornecedor.Name = "vl_fornecedor";
            // 
            // dt_paga
            // 
            this.dt_paga.DataPropertyName = "vltransporte";
            this.dt_paga.HeaderText = "Vale transporte";
            this.dt_paga.Name = "dt_paga";
            this.dt_paga.Width = 150;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "";
            this.Column2.Image = global::Nsf.trabalho.Properties.Resources.x_1152114_960_720;
            this.Column2.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.Column2.Name = "Column2";
            this.Column2.Width = 25;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Gold;
            this.button4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DeepPink;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Black;
            this.button4.Location = new System.Drawing.Point(729, 66);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 26);
            this.button4.TabIndex = 47;
            this.button4.Text = "Procurar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtFolhaPagamento
            // 
            this.txtFolhaPagamento.BackColor = System.Drawing.Color.Black;
            this.txtFolhaPagamento.ForeColor = System.Drawing.Color.White;
            this.txtFolhaPagamento.Location = new System.Drawing.Point(263, 70);
            this.txtFolhaPagamento.Name = "txtFolhaPagamento";
            this.txtFolhaPagamento.Size = new System.Drawing.Size(402, 20);
            this.txtFolhaPagamento.TabIndex = 46;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(14, 70);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(219, 21);
            this.label15.TabIndex = 45;
            this.label15.Text = "Consultar Pagamento";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Poor Richard", 27.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(266, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(318, 44);
            this.label1.TabIndex = 126;
            this.label1.Text = "Folha de Pagamento";
            // 
            // frmConsultarFolhadePagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(833, 479);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.dgvFolhadePagamento);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.txtFolhaPagamento);
            this.Controls.Add(this.label15);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmConsultarFolhadePagamento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmConsultarFolhadePagamento";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFolhadePagamento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dgvFolhadePagamento;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtFolhaPagamento;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ID_Folha_Pagamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_funcionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn nu_horas;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_salario_hora;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_total;
        private System.Windows.Forms.DataGridViewTextBoxColumn nm_empresa;
        private System.Windows.Forms.DataGridViewTextBoxColumn vl_fornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn dt_paga;
        private System.Windows.Forms.DataGridViewImageColumn Column2;
    }
}