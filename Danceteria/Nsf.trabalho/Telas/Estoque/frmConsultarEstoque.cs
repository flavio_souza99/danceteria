﻿using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarEstoque : Form
    {
        public frmConsultarEstoque()
        {
            InitializeComponent();
            CarregarCombo();
        }

        void CarregarCombo()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoDTO.IdProduto);
            cboProduto.DisplayMember = nameof(ProdutoDTO.descricaoproduto);
            cboProduto.DataSource = lista;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void frmConsultarEstoque_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgvEstoque_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            txtId.Text = prod.IdProduto.ToString();
            txtQuantidade.Text = prod.quantidade.ToString();
            txtvalor.Text = prod.valorproduto.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ProdutoDTO Pro = cboProduto.SelectedItem as ProdutoDTO;

            ProdutoDTO dto = new ProdutoDTO();


            dto.IdProduto = Convert.ToInt32(txtId.Text.Trim());
            dto.descricaoproduto = Convert.ToString(cboProduto.Text.Trim());
            dto.valorproduto = dto.valorproduto = Convert.ToDecimal(txtvalor.Text.Trim());
            dto.quantidade = Convert.ToInt32(txtQuantidade.Text);


            ProdutoDataBase business = new ProdutoDataBase();
            business.Alterar(dto);
            MessageBox.Show("Estoque alterado com sucesso.", "Blackout", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtProduto.Text.Trim());

            dgvProduto.AutoGenerateColumns = false;
            dgvProduto.DataSource = lista;
        }
    }
}
