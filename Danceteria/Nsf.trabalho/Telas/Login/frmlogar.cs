﻿using Nsf.trabalho.BancoDeDados;
using Nsf.trabalho.BD_Login_class;
using Nsf.trabalho.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmlogar : Form
    {
        public frmlogar()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            
        }
       
        private void button4_Click(object sender, EventArgs e)
        {

            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtemail.Text, txtsenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    frmMenu menu = new frmMenu();
                    menu.Show();
                    this.Hide();
                }

                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Blackout", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }      

            }
           
            catch (ArgumentException)
            {
                MessageBox.Show("Preencha os dois campos para logar.", "Blackout",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void frmlogar_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtnomec1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtnomecl_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void panelRight_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtsenhac1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void txtsenhal1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            frmCadastrarFuncionario cadastrar = new frmCadastrarFuncionario();
            this.Hide();
            cadastrar.ShowDialog();          
        }


        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtemail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtsenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetterOrDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtemail_Click(object sender, EventArgs e)
        {
            if (txtemail.Text == "Digite seu email")
            {
                txtemail.Text = string.Empty;
            }
        }

        private void txtsenha_Click(object sender, EventArgs e)
        {
            if (txtsenha.Text == "00000000")
            {
                txtsenha.Text = string.Empty;
            }
        }

        private void txtsenha_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Enter(object sender, EventArgs e)
        {

        }
    }
}
