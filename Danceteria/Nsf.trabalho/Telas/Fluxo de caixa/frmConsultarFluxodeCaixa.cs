﻿using Nsf.trabalho.Fluxo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarFluxodeCaixa : Form
    {
        public frmConsultarFluxodeCaixa()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombo2();
        }
        void CarregarCombo()
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxodeCaixaDTO> lista = business.Listar();

            cboId.ValueMember = nameof(FluxodeCaixaConsultarView.Id);
            cboId.DisplayMember = nameof(FluxodeCaixaConsultarView.Id);
            cboId.DataSource = lista;
        }

        void CarregarCombo2()
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxoCaixaDTO2> lista = business.ListarVenda();

            cboIdvenda.ValueMember = nameof(FluxoCaixaConsultarView2.Id);
            cboIdvenda.DisplayMember = nameof(FluxoCaixaConsultarView2.Id);
            cboIdvenda.DataSource = lista;
        }


        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void frmConsultarFluxodeCaixa_Load(object sender, EventArgs e)
        {
             lblSaldo.Text = Convert.ToString(Convert.ToDecimal(lblPagamento.Text) + Convert.ToDecimal(lblExtra1.Text) + Convert.ToDecimal(lblExtra2.Text) + Convert.ToDecimal(lblRefeicao.Text) + Convert.ToDecimal(lblTransporte.Text));
             lblLucro.Text = Convert.ToString(Convert.ToDecimal(lblVenda.Text) - Convert.ToDecimal(lblSaldo.Text));
        }

        private void dgvFluxodeCaixa_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
         
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            FluxodeCaixaDTO Pro = cboId.SelectedItem as FluxodeCaixaDTO;


            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            FluxodeCaixaConsultarView prod = business.ListarPorId(Pro.Id);

            lblPagamento.Text = prod.Pagamento.ToString();
            lblExtra1.Text = prod.horaextra1.ToString();
            lblExtra2.Text = prod.horaextra2.ToString();
            lblRefeicao.Text = prod.refeicao.ToString();
            lblTransporte.Text = prod.transporte.ToString();

        }

        private void cboIdvenda_SelectedIndexChanged(object sender, EventArgs e)
        {
            FluxoCaixaDTO2 Pro2 = cboIdvenda.SelectedItem as FluxoCaixaDTO2;

            FluxodeCaixaBusiness business2 = new FluxodeCaixaBusiness();
            FluxoCaixaConsultarView2 prod2 = business2.ListarPorIdVenda(Pro2.Id);

            lblVenda.Text = prod2.Venda.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void lblTransporte_Click(object sender, EventArgs e)
        {

        }

        private void panel10_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
