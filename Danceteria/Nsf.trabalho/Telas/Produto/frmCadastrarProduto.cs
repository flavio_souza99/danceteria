﻿using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmCadastrarProdutos : Form
    {
        public frmCadastrarProdutos()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void frmCadastrarProdutos_Load(object sender, EventArgs e)
        {

        }

        private void label16_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {

                ProdutoDTO dto = new ProdutoDTO();

                dto.descricaoproduto = txtdescricaoproduto.Text.Trim();
                dto.valorproduto = Convert.ToDecimal(txtvalor.Text.Trim());
                dto.quantidade = Convert.ToInt32(txtQuantidade.Text.Trim());

                if (dto.descricaoproduto == string.Empty || dto.descricaoproduto == "Informe o nome do produto")
                {
                    MessageBox.Show("Produto é obrigatório.");
                    return;
                }

                if (dto.valorproduto == 0 || dto.valorproduto == 0)
                {
                    MessageBox.Show("Valor é obrigatório.");
                    return;
                }

                if (dto.quantidade == 0 || dto.quantidade == 0)
                {
                    MessageBox.Show("Insira uma quantidade.");
                    return;
                }

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto salvo.", "Blackout",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();

                frmMenu ir = new frmMenu();
                this.Hide();
                ir.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Informações invalidas!");
                return;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void txtvalor_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdescricaoproduto_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtdescricaoproduto_Click(object sender, EventArgs e)
        {
            if (txtdescricaoproduto.Text == "Nome do produto")
            {
                txtdescricaoproduto.Text = string.Empty;
            }
        }

        private void txtvalor_Click(object sender, EventArgs e)
        {
            if (txtvalor.Text == "Digite o valor")
            {
                txtvalor.Text = string.Empty;
            }
        }

        private void txtvalor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click_1(object sender, EventArgs e)
        {

        }
    }
}
