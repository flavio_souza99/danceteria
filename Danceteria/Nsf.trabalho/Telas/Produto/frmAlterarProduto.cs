﻿using Nsf.trabalho.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Produto
{
    public partial class frmAlterarProduto : Form
    {
        public frmAlterarProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }
        void CarregarCombo()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            txtId.ValueMember = nameof(ProdutoDTO.IdProduto);
            txtId.DisplayMember = nameof(ProdutoDTO.IdProduto);
            txtId.DataSource = lista;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = new ProdutoDTO();


                dto.IdProduto = Convert.ToInt32(txtId.Text.Trim());
                dto.descricaoproduto = txtdescricaoproduto.Text.Trim();
                dto.valorproduto = Convert.ToDecimal(txtvalor.Text.Trim());
                dto.quantidade = Convert.ToInt32(txtQuantidade.Text.Trim());

                if (dto.descricaoproduto == string.Empty || dto.descricaoproduto == "Informe o nome do produto")
                {
                    MessageBox.Show("Produto é obrigatório.");
                    return;
                }

                if (dto.valorproduto == 0 || dto.valorproduto == 0)
                {
                    MessageBox.Show("Valor é obrigatório.");
                    return;
                }

                if (dto.quantidade == 0 || dto.quantidade == 0)
                {
                    MessageBox.Show("Insira uma quantidade.");
                    return;
                }


                ProdutoDataBase db = new ProdutoDataBase();
                db.Alterar(dto);

                MessageBox.Show("Informação alterada com sucesso.", "Blackout",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);

            }
            catch (Exception)
            {
                MessageBox.Show("Tente novamente!");
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtdescricaoproduto_Click(object sender, EventArgs e)
        {
            if (txtdescricaoproduto.Text == "Nome do produto")
            {
                txtdescricaoproduto.Text = string.Empty;
            }
        }

        private void txtvalor_Click(object sender, EventArgs e)
        {
            if (txtvalor.Text == "Digite o valor")
            {
                txtvalor.Text = string.Empty;
            }
        }

        private void txtvalor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsPunctuation(e.KeyChar) || char.IsSymbol(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void frmAlterarProduto_Load(object sender, EventArgs e)
        {

        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {                             
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtId_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProdutoDTO Pro = txtId.SelectedItem as ProdutoDTO;

            ProdutoBusiness business = new ProdutoBusiness();
            ProdutoDTO prod = business.ListarPorId(Pro.IdProduto);

            txtdescricaoproduto.Text = prod.descricaoproduto.ToString();
            txtvalor.Text = prod.valorproduto.ToString();
            txtQuantidade.Text = prod.quantidade.ToString();
        }
    }
}
