﻿using Nsf.trabalho.API.Correios;
using Nsf.trabalho.Fornecedor;
using Nsf.trabalho.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho.Telas.Fornecedor
{
    public partial class frmAlterarFornecedor : Form
    {
        public frmAlterarFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombos();
        }


        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            cboId.ValueMember = nameof(FornecedorDTO.IdFornecedor);
            cboId.DisplayMember = nameof(FornecedorDTO.IdFornecedor);
            cboId.DataSource = lista;
        }


        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.IdFuncionario);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.funcionario);
            cboFuncionario.DataSource = lista;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO Fun = cboFuncionario.SelectedItem as FuncionarioDTO;

                FornecedorDTO dto = new FornecedorDTO();
                dto.IdFornecedor = Convert.ToInt32(cboId.Text.Trim());
                dto.FuncionarioId = Fun.IdFuncionario;
                dto.empresa = txtnome.Text.Trim();
                dto.cnpj = txtCNPJ.Text;
                dto.inscricao = txtInscricao.Text.Trim();
                dto.rua = txtRua.Text.Trim();
                dto.bairro = txtBairro.Text.Trim();
                dto.local = txtNumero.Text.Trim();
                dto.cep = txtCep.Text.Trim();
                dto.estado = txtEstado.Text.Trim();
                dto.uf = txtUF.Text.Trim();
                dto.tel = txtTelefone.Text.Trim();
                dto.email = txtEmail.Text.Trim();

                if (dto.empresa == string.Empty || dto.empresa == "Empresa")
                {
                    MessageBox.Show("Digite o nome da empresa");

                    return;
                }
                if (dto.cnpj == string.Empty || txtCNPJ.Text.Length <= 17)
                {
                    MessageBox.Show("Digite seu CNPJ");

                    return;
                }
                if (dto.inscricao == string.Empty || dto.inscricao == "Digite sua inscrição" || txtInscricao.Text.Length <= 11)
                {
                    MessageBox.Show("Digite sua inscrição");

                    return;
                }

                if (dto.rua == string.Empty || dto.rua == "Digite sua rua")
                {
                    MessageBox.Show("Digite sua rua");

                    return;
                }

                if (dto.bairro == string.Empty || dto.bairro == "Bairro")
                {
                    MessageBox.Show("Digite seu bairro");

                    return;
                }


                if (dto.local == string.Empty || dto.local == "Digite o numero da sua casa")
                {
                    MessageBox.Show("Digite o numero da sua casa");

                    return;
                }

                if (dto.cep == string.Empty || dto.cep == "Digite seu cep" || txtCep.Text.Length <= 8)
                {
                    MessageBox.Show("Digite o cep");

                    return;
                }

                if (dto.estado == string.Empty || dto.estado == "Digite seu estado")
                {
                    MessageBox.Show("Digite sua estado");

                    return;
                }

                if (dto.uf == string.Empty || txtUF.Text.Length <= 1)
                {
                    MessageBox.Show("Digitar uf");

                    return;
                }

                if (dto.tel == string.Empty || dto.tel == "Digite seu telefone" || txtTelefone.Text.Length <= 13)
                {
                    MessageBox.Show("Digite o telefone");

                    return;
                }


                if (dto.email == string.Empty || dto.email == "Digite seu e-mail")
                {
                    MessageBox.Show("Digite o seu email");

                    return;
                }

                FornecedorBusiness business = new FornecedorBusiness();
                business.Alterar(dto);

                MessageBox.Show("Fornecedor Alterado com sucesso.", "Salvo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {
                MessageBox.Show("Informações invalidas tente novamente");
                return;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtId_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtEstado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || char.IsControl(e.KeyChar) || char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }

            else
            {
                e.Handled = true;
            }
        }

        private void txtnome_Click(object sender, EventArgs e)
        {
            if (txtnome.Text == "Nome")
            {
                txtnome.Text = string.Empty;
            }
        }

        private void txtRua_Click(object sender, EventArgs e)
        {
            if (txtRua.Text == "Rua")
            {
                txtRua.Text = string.Empty;
            }
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            if (txtBairro.Text == "Bairro")
            {
                txtBairro.Text = string.Empty;
            }
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {
            if (txtNumero.Text == "Nº")
            {
                txtNumero.Text = string.Empty;
            }
        }

        private void txtEstado_Click(object sender, EventArgs e)
        {
            if (txtEstado.Text == "Estado do fornecedor")
            {
                txtEstado.Text = string.Empty;
            }
        }

        private void txtEmail_Click(object sender, EventArgs e)
        {
            if (txtEmail.Text == "Digite seu e-mail")
            {
                txtEmail.Text = string.Empty;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Correio correio = new Correio();
                CorreioResponse dto = correio.Endereco(txtCep.Text);
                txtRua.Text = dto.Logradouro;
                txtBairro.Text = dto.Bairro;
                txtEstado.Text = dto.Localidade;
                txtUF.Text = dto.UF;

            }
            catch (Exception)
            {
                MessageBox.Show("Cep Inválido");
                return;
            }
        }

        private void cboId_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedorDTO Pro = cboId.SelectedItem as FornecedorDTO;

            FornecedorBusiness business = new FornecedorBusiness();
            FornecedorDTO prod = business.ListarPorId(Pro.IdFornecedor);

            txtnome.Text = prod.empresa.ToString();
            txtCNPJ.Text = prod.cnpj.ToString();
            txtInscricao.Text = prod.inscricao.ToString();
            txtRua.Text = prod.rua.ToString();
            txtBairro.Text = prod.bairro.ToString();
            txtNumero.Text = prod.local.ToString();
            txtCep.Text = prod.cep.ToString();
            txtEstado.Text = prod.estado.ToString();
            txtUF.Text = prod.uf.ToString();
            txtTelefone.Text = prod.tel.ToString();
            txtEmail.Text = prod.email.ToString();
        }
    }
}
