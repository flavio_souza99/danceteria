﻿using Nsf.trabalho.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmConsultarFornecedor : Form
    {
        public frmConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void frmConsultarFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu voltar = new frmMenu();
            this.Hide();
            voltar.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorConsultarView> lista = business.Consultar(txtFornecedor.Text);

            dgvFornecedor.AutoGenerateColumns = false;
            dgvFornecedor.DataSource = lista;
        }

        private void dgvFornecedor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 13)
                {
                    FornecedorConsultarView cat = dgvFornecedor.CurrentRow.DataBoundItem as FornecedorConsultarView;

                    FornecedorBusiness business = new FornecedorBusiness();
                    business.Remover(cat.Id);

                    button4_Click(null, null);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Esse fornecedor não pode ser removido agora");
                frmConsultarFornecedor ir = new frmConsultarFornecedor();
                return;
            }
        }
    }
}
