﻿namespace Nsf.trabalho
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoufToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minimizarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fecharToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.Hora = new System.Windows.Forms.Timer(this.components);
            this.lblData = new System.Windows.Forms.Label();
            this.lblData2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.produtoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.clienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.estoqueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pedidoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarPedidoAoFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarPedidoFeitoAoFornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.realizarPedidoAoFornececedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folhaDePagamentoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.fornecedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fluxoDeCaixaToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.funcionarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.logoufToolStripMenuItem,
            this.minimizarToolStripMenuItem,
            this.fecharToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(486, 24);
            this.menuStrip1.TabIndex = 54;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.produtoToolStripMenuItem,
            this.clienteToolStripMenuItem,
            this.estoqueToolStripMenuItem,
            this.pedidoToolStripMenuItem1,
            this.folhaDePagamentoToolStripMenuItem,
            this.fornecedorToolStripMenuItem,
            this.fluxoDeCaixaToolStripMenuItem1,
            this.funcionarioToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            this.menuToolStripMenuItem.Click += new System.EventHandler(this.menuToolStripMenuItem_Click);
            // 
            // logoufToolStripMenuItem
            // 
            this.logoufToolStripMenuItem.Name = "logoufToolStripMenuItem";
            this.logoufToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.logoufToolStripMenuItem.Text = "Logouf";
            this.logoufToolStripMenuItem.Click += new System.EventHandler(this.logoufToolStripMenuItem_Click);
            // 
            // minimizarToolStripMenuItem
            // 
            this.minimizarToolStripMenuItem.Name = "minimizarToolStripMenuItem";
            this.minimizarToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.minimizarToolStripMenuItem.Text = "Minimizar";
            this.minimizarToolStripMenuItem.Click += new System.EventHandler(this.minimizarToolStripMenuItem_Click);
            // 
            // fecharToolStripMenuItem
            // 
            this.fecharToolStripMenuItem.Name = "fecharToolStripMenuItem";
            this.fecharToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fecharToolStripMenuItem.Text = "Fechar";
            this.fecharToolStripMenuItem.Click += new System.EventHandler(this.fecharToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(465, 21);
            this.label1.TabIndex = 56;
            this.label1.Text = "“De um blackout no stress e acenda a diversão”";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(199, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 34);
            this.label2.TabIndex = 57;
            this.label2.Text = "Menu";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.BackColor = System.Drawing.Color.Transparent;
            this.lblHora.Font = new System.Drawing.Font("Cooper Black", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.White;
            this.lblHora.Location = new System.Drawing.Point(181, 280);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(0, 34);
            this.lblHora.TabIndex = 58;
            // 
            // Hora
            // 
            this.Hora.Enabled = true;
            this.Hora.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.BackColor = System.Drawing.Color.Transparent;
            this.lblData.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.White;
            this.lblData.Location = new System.Drawing.Point(94, 324);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(0, 21);
            this.lblData.TabIndex = 59;
            this.lblData.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblData2
            // 
            this.lblData2.AutoSize = true;
            this.lblData2.Font = new System.Drawing.Font("Cooper Black", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData2.ForeColor = System.Drawing.Color.Transparent;
            this.lblData2.Location = new System.Drawing.Point(28, 324);
            this.lblData2.Name = "lblData2";
            this.lblData2.Size = new System.Drawing.Size(19, 27);
            this.lblData2.TabIndex = 60;
            this.lblData2.Text = ":";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Nsf.trabalho.Properties.Resources._1519997058_loader_1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(486, 348);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 55;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // produtoToolStripMenuItem
            // 
            this.produtoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pedidoToolStripMenuItem,
            this.consultarToolStripMenuItem,
            this.alterarToolStripMenuItem6});
            this.produtoToolStripMenuItem.Image = global::Nsf.trabalho.Properties.Resources.AddProduto;
            this.produtoToolStripMenuItem.Name = "produtoToolStripMenuItem";
            this.produtoToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.produtoToolStripMenuItem.Text = "Produto";
            this.produtoToolStripMenuItem.Click += new System.EventHandler(this.produtoToolStripMenuItem_Click);
            // 
            // pedidoToolStripMenuItem
            // 
            this.pedidoToolStripMenuItem.Name = "pedidoToolStripMenuItem";
            this.pedidoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pedidoToolStripMenuItem.Text = "Cadastrar";
            this.pedidoToolStripMenuItem.Click += new System.EventHandler(this.pedidoToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.consultarToolStripMenuItem.Text = "Consultar";
            this.consultarToolStripMenuItem.Click += new System.EventHandler(this.consultarToolStripMenuItem_Click);
            // 
            // alterarToolStripMenuItem6
            // 
            this.alterarToolStripMenuItem6.Name = "alterarToolStripMenuItem6";
            this.alterarToolStripMenuItem6.Size = new System.Drawing.Size(180, 22);
            this.alterarToolStripMenuItem6.Text = "Alterar";
            this.alterarToolStripMenuItem6.Click += new System.EventHandler(this.alterarToolStripMenuItem6_Click);
            // 
            // clienteToolStripMenuItem
            // 
            this.clienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.consultarToolStripMenuItem1,
            this.alterarToolStripMenuItem5});
            this.clienteToolStripMenuItem.Image = global::Nsf.trabalho.Properties.Resources.Iconos_clientes_01;
            this.clienteToolStripMenuItem.Name = "clienteToolStripMenuItem";
            this.clienteToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.clienteToolStripMenuItem.Text = "Cliente";
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem.Click += new System.EventHandler(this.cadastrarToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // alterarToolStripMenuItem5
            // 
            this.alterarToolStripMenuItem5.Name = "alterarToolStripMenuItem5";
            this.alterarToolStripMenuItem5.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem5.Text = "Alterar";
            this.alterarToolStripMenuItem5.Click += new System.EventHandler(this.alterarToolStripMenuItem5_Click);
            // 
            // estoqueToolStripMenuItem
            // 
            this.estoqueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem2});
            this.estoqueToolStripMenuItem.Image = global::Nsf.trabalho.Properties.Resources.icon_controle_de_estoqu;
            this.estoqueToolStripMenuItem.Name = "estoqueToolStripMenuItem";
            this.estoqueToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.estoqueToolStripMenuItem.Text = "Estoque";
            // 
            // consultarToolStripMenuItem2
            // 
            this.consultarToolStripMenuItem2.Name = "consultarToolStripMenuItem2";
            this.consultarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem2.Text = "Consultar";
            this.consultarToolStripMenuItem2.Click += new System.EventHandler(this.consultarToolStripMenuItem2_Click);
            // 
            // pedidoToolStripMenuItem1
            // 
            this.pedidoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem2,
            this.consultarToolStripMenuItem3,
            this.alterarToolStripMenuItem3,
            this.consultarPedidoAoFornecedorToolStripMenuItem,
            this.alterarPedidoFeitoAoFornecedorToolStripMenuItem,
            this.realizarPedidoAoFornececedorToolStripMenuItem});
            this.pedidoToolStripMenuItem1.Image = global::Nsf.trabalho.Properties.Resources._1f6d2;
            this.pedidoToolStripMenuItem1.Name = "pedidoToolStripMenuItem1";
            this.pedidoToolStripMenuItem1.Size = new System.Drawing.Size(183, 22);
            this.pedidoToolStripMenuItem1.Text = "Pedido";
            // 
            // cadastrarToolStripMenuItem2
            // 
            this.cadastrarToolStripMenuItem2.Name = "cadastrarToolStripMenuItem2";
            this.cadastrarToolStripMenuItem2.Size = new System.Drawing.Size(253, 22);
            this.cadastrarToolStripMenuItem2.Text = "Cadastrar pedido do cliente";
            this.cadastrarToolStripMenuItem2.Click += new System.EventHandler(this.cadastrarToolStripMenuItem2_Click);
            // 
            // consultarToolStripMenuItem3
            // 
            this.consultarToolStripMenuItem3.Name = "consultarToolStripMenuItem3";
            this.consultarToolStripMenuItem3.Size = new System.Drawing.Size(253, 22);
            this.consultarToolStripMenuItem3.Text = "Consultar pedido do cliente";
            this.consultarToolStripMenuItem3.Click += new System.EventHandler(this.consultarToolStripMenuItem3_Click);
            // 
            // alterarToolStripMenuItem3
            // 
            this.alterarToolStripMenuItem3.Name = "alterarToolStripMenuItem3";
            this.alterarToolStripMenuItem3.Size = new System.Drawing.Size(253, 22);
            this.alterarToolStripMenuItem3.Text = "Alterar pedido do cliente";
            this.alterarToolStripMenuItem3.Click += new System.EventHandler(this.alterarToolStripMenuItem3_Click);
            // 
            // consultarPedidoAoFornecedorToolStripMenuItem
            // 
            this.consultarPedidoAoFornecedorToolStripMenuItem.Name = "consultarPedidoAoFornecedorToolStripMenuItem";
            this.consultarPedidoAoFornecedorToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.consultarPedidoAoFornecedorToolStripMenuItem.Text = "Consultar pedido ao fornecedor";
            this.consultarPedidoAoFornecedorToolStripMenuItem.Click += new System.EventHandler(this.consultarPedidoAoFornecedorToolStripMenuItem_Click);
            // 
            // alterarPedidoFeitoAoFornecedorToolStripMenuItem
            // 
            this.alterarPedidoFeitoAoFornecedorToolStripMenuItem.Name = "alterarPedidoFeitoAoFornecedorToolStripMenuItem";
            this.alterarPedidoFeitoAoFornecedorToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.alterarPedidoFeitoAoFornecedorToolStripMenuItem.Text = "Alterar pedido feito ao fornecedor";
            this.alterarPedidoFeitoAoFornecedorToolStripMenuItem.Click += new System.EventHandler(this.alterarPedidoFeitoAoFornecedorToolStripMenuItem_Click);
            // 
            // realizarPedidoAoFornececedorToolStripMenuItem
            // 
            this.realizarPedidoAoFornececedorToolStripMenuItem.Name = "realizarPedidoAoFornececedorToolStripMenuItem";
            this.realizarPedidoAoFornececedorToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.realizarPedidoAoFornececedorToolStripMenuItem.Text = "Realizar pedido ao fornececedor";
            this.realizarPedidoAoFornececedorToolStripMenuItem.Click += new System.EventHandler(this.realizarPedidoAoFornececedorToolStripMenuItem_Click);
            // 
            // folhaDePagamentoToolStripMenuItem
            // 
            this.folhaDePagamentoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem3,
            this.consultarToolStripMenuItem4,
            this.alterarToolStripMenuItem2});
            this.folhaDePagamentoToolStripMenuItem.Image = global::Nsf.trabalho.Properties.Resources.e928306ecfe83395ec9d65c8fe502226_moedas_de_dinheiro_by_vexels;
            this.folhaDePagamentoToolStripMenuItem.Name = "folhaDePagamentoToolStripMenuItem";
            this.folhaDePagamentoToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.folhaDePagamentoToolStripMenuItem.Text = "Folha de Pagamento";
            // 
            // cadastrarToolStripMenuItem3
            // 
            this.cadastrarToolStripMenuItem3.Name = "cadastrarToolStripMenuItem3";
            this.cadastrarToolStripMenuItem3.Size = new System.Drawing.Size(125, 22);
            this.cadastrarToolStripMenuItem3.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem3.Click += new System.EventHandler(this.cadastrarToolStripMenuItem3_Click);
            // 
            // consultarToolStripMenuItem4
            // 
            this.consultarToolStripMenuItem4.Name = "consultarToolStripMenuItem4";
            this.consultarToolStripMenuItem4.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem4.Text = "Consultar";
            this.consultarToolStripMenuItem4.Click += new System.EventHandler(this.consultarToolStripMenuItem4_Click);
            // 
            // alterarToolStripMenuItem2
            // 
            this.alterarToolStripMenuItem2.Name = "alterarToolStripMenuItem2";
            this.alterarToolStripMenuItem2.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem2.Text = "Alterar";
            this.alterarToolStripMenuItem2.Click += new System.EventHandler(this.alterarToolStripMenuItem2_Click);
            // 
            // fornecedorToolStripMenuItem
            // 
            this.fornecedorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem4,
            this.consultarToolStripMenuItem5,
            this.alterarToolStripMenuItem1});
            this.fornecedorToolStripMenuItem.Image = global::Nsf.trabalho.Properties.Resources.kisspng_computer_icons_vendor_delivery_5ac0895b6b5bb1_3621282915225675154398;
            this.fornecedorToolStripMenuItem.Name = "fornecedorToolStripMenuItem";
            this.fornecedorToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.fornecedorToolStripMenuItem.Text = "Fornecedor";
            this.fornecedorToolStripMenuItem.Click += new System.EventHandler(this.fornecedorToolStripMenuItem_Click);
            // 
            // cadastrarToolStripMenuItem4
            // 
            this.cadastrarToolStripMenuItem4.Name = "cadastrarToolStripMenuItem4";
            this.cadastrarToolStripMenuItem4.Size = new System.Drawing.Size(180, 22);
            this.cadastrarToolStripMenuItem4.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem4.Click += new System.EventHandler(this.cadastrarToolStripMenuItem4_Click);
            // 
            // consultarToolStripMenuItem5
            // 
            this.consultarToolStripMenuItem5.Name = "consultarToolStripMenuItem5";
            this.consultarToolStripMenuItem5.Size = new System.Drawing.Size(180, 22);
            this.consultarToolStripMenuItem5.Text = "Consultar";
            this.consultarToolStripMenuItem5.Click += new System.EventHandler(this.consultarToolStripMenuItem5_Click);
            // 
            // alterarToolStripMenuItem1
            // 
            this.alterarToolStripMenuItem1.Name = "alterarToolStripMenuItem1";
            this.alterarToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.alterarToolStripMenuItem1.Text = "Alterar";
            this.alterarToolStripMenuItem1.Click += new System.EventHandler(this.alterarToolStripMenuItem1_Click);
            // 
            // fluxoDeCaixaToolStripMenuItem1
            // 
            this.fluxoDeCaixaToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem6});
            this.fluxoDeCaixaToolStripMenuItem1.Image = global::Nsf.trabalho.Properties.Resources.fluxo_caixa;
            this.fluxoDeCaixaToolStripMenuItem1.Name = "fluxoDeCaixaToolStripMenuItem1";
            this.fluxoDeCaixaToolStripMenuItem1.Size = new System.Drawing.Size(183, 22);
            this.fluxoDeCaixaToolStripMenuItem1.Text = "Fluxo de caixa";
            // 
            // consultarToolStripMenuItem6
            // 
            this.consultarToolStripMenuItem6.Name = "consultarToolStripMenuItem6";
            this.consultarToolStripMenuItem6.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem6.Text = "Consultar";
            this.consultarToolStripMenuItem6.Click += new System.EventHandler(this.consultarToolStripMenuItem6_Click);
            // 
            // funcionarioToolStripMenuItem
            // 
            this.funcionarioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consultarToolStripMenuItem7,
            this.alterarToolStripMenuItem7});
            this.funcionarioToolStripMenuItem.Image = global::Nsf.trabalho.Properties.Resources.engineer;
            this.funcionarioToolStripMenuItem.Name = "funcionarioToolStripMenuItem";
            this.funcionarioToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.funcionarioToolStripMenuItem.Text = "Funcionario";
            // 
            // consultarToolStripMenuItem7
            // 
            this.consultarToolStripMenuItem7.Name = "consultarToolStripMenuItem7";
            this.consultarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.consultarToolStripMenuItem7.Text = "Consultar";
            this.consultarToolStripMenuItem7.Click += new System.EventHandler(this.consultarToolStripMenuItem7_Click);
            // 
            // alterarToolStripMenuItem7
            // 
            this.alterarToolStripMenuItem7.Name = "alterarToolStripMenuItem7";
            this.alterarToolStripMenuItem7.Size = new System.Drawing.Size(125, 22);
            this.alterarToolStripMenuItem7.Text = "Alterar";
            this.alterarToolStripMenuItem7.Click += new System.EventHandler(this.alterarToolStripMenuItem7_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(486, 372);
            this.Controls.Add(this.lblData2);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblHora);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.RoyalBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPedidoCadastrar";
            this.Load += new System.EventHandler(this.frmPedidoCadastrar_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produtoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem estoqueToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem pedidoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem folhaDePagamentoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem fornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fluxoDeCaixaToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem logoufToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minimizarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fecharToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem funcionarioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem consultarPedidoAoFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarPedidoFeitoAoFornecedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem realizarPedidoAoFornececedorToolStripMenuItem;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Timer Hora;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblData2;
    }
}