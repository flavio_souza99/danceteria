﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.trabalho
{
    public partial class frmSplash : Form
    {
        public frmSplash()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {
                System.Threading.Thread.Sleep(3000);
                Invoke(new Action(() =>
                {
                    frmlogar frm = new frmlogar();
                    frm.Show();
                    Hide();
                }));
            });

        }

        private void frmSplash_Load(object sender, EventArgs e)
        {

        }
    }
}
