﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.trabalho.BancoDeDados
{
    public class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=danceteria;uid=root;password=1234;sslmode=none";
            //string connectionString = "server=70.37.57.127;database=danceteria;uid=nsf;password=nsf@2018;sslmode=none";
            //string connectionString = "server=192.168.0.100;database=danceteria;uid=nsf;password=nsf@2018;sslmode=none";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            return connection;
        }
    }
}
